#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.basemap import Basemap
import scipy
from scipy import signal
from scipy import *
from scipy import ndimage
import skimage
from skimage.morphology import label
from skimage.measure import regionprops
import math
from math import radians, tan, sin, cos, pi, atan, sqrt, pow, asin, acos
import pylab as P
import numpy as np
from numpy import NAN
import sys
import netCDF4
from optparse import OptionParser
from netcdftime import utime
import os
import time as timeit
from optparse import OptionParser
import news_e_post_cbook
from news_e_post_cbook import *
import news_e_plotting_cbook_v2
from news_e_plotting_cbook_v2 import *
import radar_info
from radar_info import *

####################################### Parse Options: ######################################################

parser = OptionParser()
parser.add_option("-d", dest="exp_dir", type="string", default= None, help="Input Directory (of MRMS files)")
parser.add_option("-z", dest="dz_dir", type="string", default= None, help="Input Directory (of MRMS dBZ files)")
parser.add_option("-o", dest="out_path", type="string", help = "Output File Path")
parser.add_option("-f", dest="newse_path", type="string", help = "Path to NEWS-e Summary File")
parser.add_option("-v", dest="var", type="string", help = "MRMS variable name (e.g. MergedReflectivityQCComposite)")
parser.add_option("-s", dest="start_t", type="string", help = "Start Time (hhmmss)")
parser.add_option("-e", dest="end_t", type="string", help = "End Time (hhmmss)")

(options, args) = parser.parse_args()

if ((options.exp_dir == None) or (options.dz_dir == None) or (options.out_path == None) or (options.newse_path == None) or (options.var == None) or (options.start_t == None) or (options.end_t == None)):
    print
    parser.print_help()
    print
    sys.exit(1)
else:
    exp_dir = options.exp_dir
    dz_dir = options.dz_dir
    out_path = options.out_path
    newse_path = options.newse_path
    var = options.var
    start_t = options.start_t
    end_t = options.end_t

#################################### Create time bins for analyses:  #####################################################

start_t_in_seconds = double(start_t[0:2]) * 3600. + double(start_t[2:4]) * 60. + double(start_t[4:6])
end_t_in_seconds = double(end_t[0:2]) * 3600. + double(end_t[2:4]) * 60. + double(end_t[4:6])

if (double(start_t[0:2]) < 10):
   start_t_in_seconds = start_t_in_seconds + 86400.
if (double(end_t[0:2]) < 10):
   end_t_in_seconds = end_t_in_seconds + 86400.

times = np.arange(start_t_in_seconds, end_t_in_seconds, 300.)   ## 1900 - 0500 UTC in seconds, every 5 min

#################################### Specify OBAN/qc thresholds for DZ/Az shear:  #####################################################

if (var[-1] == 'e'):  #set min threshold to 5 dbz and 0 s^-1, depending on var
   var_thresh = 5.
   min_obs         = 4
else: 
   var_thresh = 0.
   min_obs         = 4

#################################### Generic Basemap Variables (to call as quickly as possible):  #####################################################

damage_files     = ''
area_thresh      = 1000.
resolution       = 'c'

#################################### User-Defined Variables:  #####################################################

dz_thresh        = 40.
dz_rad		 = 25000. 
roi              = 3000.  		#radius of influence for Cressman scheme (m)
#range_min       = 15000.               #radius near radar to remove
range_min       = 5000.                 #radius near radar to remove
range_max       = 150000.               #radius away radar to remove
radius_max      = 3             	#grid point radius for maximum value filter
radius_gauss    = 2       	        #grid point radius of convolution operator
time_window     = 3
aws_thresh      = 0.004
area_thresh2    = 2
eccent_thresh   = 0.4
kernel = gauss_kern(radius_gauss)

######################################################################################################
#################################### Read Data:  #####################################################
######################################################################################################

################### Get MRMS DZ/Az shear files: ############################

t = 0
init = 0
mrms_files = []
mrms_files_temp = os.listdir(exp_dir)

mrms_dz_files = []
mrms_dz_files_temp = os.listdir(dz_dir)

################### Get Grid info from NEWS-e summary file: ############################

try:
   newse_in = netCDF4.Dataset(newse_path, "r")
   print "Opening %s \n" % newse_path
except:
   print "%s does not exist! \n" %newse_path
   sys.exit(1)

newse_lat = newse_in.variables["XLAT"][:]
newse_lon = newse_in.variables["XLON"][:]

sw_lat_newse = newse_lat[0,0]
sw_lon_newse = newse_lon[0,0]
ne_lat_newse = newse_lat[-1,-1]
ne_lon_newse = newse_lon[-1,-1]

cen_lat = newse_in.CEN_LAT
cen_lon = newse_in.CEN_LON
stand_lon = newse_in.STAND_LON
true_lat1 = newse_in.TRUE_LAT1
true_lat2 = newse_in.TRUE_LAT2

newse_in.close()
del newse_in

################### Create Basemap instance for grid conversion: ############################

newse_map = Basemap(llcrnrlon=sw_lon_newse, llcrnrlat=sw_lat_newse, urcrnrlon=ne_lon_newse, urcrnrlat=ne_lat_newse, projection='lcc', lat_1=true_lat1, lat_2=true_lat2, lat_0=cen_lat, lon_0=cen_lon, resolution = resolution, area_thresh = area_thresh)

newse_x_offset, newse_y_offset = newse_map(cen_lon, cen_lat)
newse_x, newse_y = newse_map(newse_lon[:], newse_lat[:])

newse_x = newse_x - newse_x_offset
newse_y = newse_y - newse_y_offset

newse_x_ravel = newse_x.ravel()
newse_y_ravel = newse_y.ravel()

################### Create KD Tree of NEWS-e x/y gridpoints: ############################

newse_x_y = np.dstack([newse_y_ravel, newse_x_ravel])[0]
newse_tree = scipy.spatial.cKDTree(newse_x_y)

################### Load locations of 88d sites from radar_sites object: ############################

rad_x, rad_y = newse_map(np.asarray(radar_sites.lon), np.asarray(radar_sites.lat))
rad_x = rad_x - newse_x_offset
rad_y = rad_y - newse_y_offset

################### Create KD Tree of radar site x/y locations: ############################

rad_x_y = np.dstack([rad_y, rad_x])[0]
rad_tree = scipy.spatial.cKDTree(rad_x_y)

################### Find points in radar blanking region: ############################

near_rad_points = newse_tree.query_ball_tree(rad_tree, range_min)
far_rad_points = newse_tree.query_ball_tree(rad_tree, range_max)

rad_mask = newse_x_ravel * 0. #initialize radmask

################### Set points in radar blanking region to 1 in radmask: ############################

for i in range(0,len(near_rad_points)):
   if (len(near_rad_points[i]) > 0.):
      rad_mask[i] = 1.

for i in range(0,len(far_rad_points)):
   if (len(far_rad_points[i]) == 0.):
      rad_mask[i] = 1.

rad_mask = rad_mask.reshape(newse_x.shape[0], newse_x.shape[1])

################### Read MRMS DZ/Az shear data: ############################

for f, file in enumerate(mrms_files_temp):
   if (file[0] == '2'):                          #assumes filename format of: "wrfout_d02_yyyy-mm-dd_hh:mm:ss
      mrms_files.append(file)

mrms_files.sort()
nt = len(mrms_files)

for f, file in enumerate(mrms_dz_files_temp):
   if (file[0] == '2'):                          #assumes filename format of: "wrfout_d02_yyyy-mm-dd_hh:mm:ss
      mrms_dz_files.append(file)

mrms_dz_files.sort()
dz_nt = len(mrms_dz_files) 

mrms_times = np.zeros((nt))
mrms_dz_times = np.zeros((dz_nt))

for f, file in enumerate(mrms_files):
   date = file[0:8]
   hour = file[9:11]
   minute = file[11:13]
   second = file[13:15]

   time_in_seconds = double(hour) * 3600. + double(minute) * 60. + double(second)

   if (double(hour) < 10): 
      time_in_seconds = time_in_seconds + 86400.

   mrms_times[f] = time_in_seconds

for f, file in enumerate(mrms_dz_files):
   date = file[0:8]
   hour = file[9:11]
   minute = file[11:13]
   second = file[13:15]

   time_in_seconds = double(hour) * 3600. + double(minute) * 60. + double(second)

   if (double(hour) < 10):
      time_in_seconds = time_in_seconds + 86400.

   mrms_dz_times[f] = time_in_seconds

################### For each time bin, interpolate nearest MRMS obs to NEWS-e grid using Cressman Scheme: ############################

for t in range(0, len(times)): 
   t_diff = np.min(np.abs(times[t] - mrms_times))
   
   print 'time difference: ', t_diff

################### Find and read MRMS DZ/Az shear file in time to current time bin: ############################

   temp_t = (np.abs(times[t] - mrms_times)).argmin()
   temp_dz_t = (np.abs(times[t] - mrms_dz_times)).argmin()
#   temp_t = (np.abs((times[t]-120.) - mrms_times)).argmin() ##hack to see difference in unused files
   temp_file = mrms_files[temp_t]
   temp_time = mrms_times[temp_t]
   temp_path = os.path.join(exp_dir, temp_file)

   temp_dz_file = mrms_dz_files[temp_dz_t]
   temp_dz_time = mrms_dz_times[temp_dz_t]
   temp_dz_path = os.path.join(dz_dir, temp_dz_file)

   try:
      fin = netCDF4.Dataset(temp_path, "r")
      print "Opening %s \n" % temp_path
   except:
      print "%s does not exist! \n" %temp_path
      sys.exit(1)

   try:
      dzin = netCDF4.Dataset(temp_dz_path, "r")
      print "Opening %s \n" % temp_dz_path
   except:
      print "%s does not exist! \n" %temp_dz_path
      sys.exit(1)

################### For first AWS file, build MRMS grid from sparse .netcdf file: ############################

   if (t == 0):
      nw_lat = fin.Latitude
      nw_lon = fin.Longitude
      lat_dy = fin.LatGridSpacing
      lon_dx = fin.LonGridSpacing
      lat_length = len(fin.dimensions["Lat"])
      lon_length = len(fin.dimensions["Lon"])

      lat_range = np.arange((nw_lat-((lat_length-1)*lat_dy)),(nw_lat+0.00001),lat_dy)
      lon_range = np.arange(nw_lon,(nw_lon+(lon_length*lon_dx)),lon_dx)
      xlon_full, xlat_full = np.meshgrid(lon_range, lat_range)

      lon_range_indices = np.arange(0,lon_length)
      lat_range_indices = np.arange(0,lat_length)
#      lat_range_indices = np.arange((lat_length-1),-1,-1)

      xlon_indices, xlat_indices = np.meshgrid(lon_range_indices, lat_range_indices)

      min_lat = (np.abs(xlat_full[:,0]-np.min(newse_lat))).argmin()
      max_lat = (np.abs(xlat_full[:,0]-np.max(newse_lat))).argmin()
      min_lon = (np.abs(xlon_full[0,:]-np.min(newse_lon))).argmin()
      max_lon = (np.abs(xlon_full[0,:]-np.max(newse_lon))).argmin()

      xlat = xlat_full[min_lat:max_lat,min_lon:max_lon]
      xlon = xlon_full[min_lat:max_lat,min_lon:max_lon]

      sw_xlat = xlat[0,0]
      sw_xlon = xlon[0,0]
      ne_xlat = xlat[-1,-1]
      ne_xlon = xlon[-1,-1]

      map = Basemap(llcrnrlon=sw_xlon, llcrnrlat=sw_xlat, urcrnrlon=ne_xlon, urcrnrlat=ne_xlat, projection='lcc', lat_1=true_lat1, lat_2=true_lat2, lat_0=cen_lat, lon_0=cen_lon, resolution = resolution, area_thresh = area_thresh)

      x_offset, y_offset = map(cen_lon, cen_lat)
      x, y = map(xlon[:], xlat[:])

      x_full, y_full = map(xlon_full[:], xlat_full[:])

      x = x - x_offset
      y = y - y_offset

      x_full = x_full - x_offset
      y_full = y_full - y_offset

      min_x, min_y = map(lon_range[min_lon], lat_range[min_lat])
      max_x, max_y = map(lon_range[max_lon], lat_range[max_lat])

      min_x = min_x - x_offset
      max_x = max_x - x_offset
      min_y = min_y - y_offset
      max_y = max_y - y_offset

################### For first DZ file, build MRMS grid from sparse .netcdf file: ############################
######### Done separately for DZ and Az. shear since they are on different grids for 2016 data ############## 

      dz_nw_lat = dzin.Latitude
      dz_nw_lon = dzin.Longitude
      dz_lat_dy = dzin.LatGridSpacing
      dz_lon_dx = dzin.LonGridSpacing
      dz_lat_length = len(dzin.dimensions["Lat"])
      dz_lon_length = len(dzin.dimensions["Lon"])

      dz_lat_range = np.arange((dz_nw_lat-((dz_lat_length-1)*dz_lat_dy)),(dz_nw_lat+0.00001),dz_lat_dy)
      dz_lon_range = np.arange(dz_nw_lon,(dz_nw_lon+(dz_lon_length*dz_lon_dx)),dz_lon_dx)
      dz_xlon_full, dz_xlat_full = np.meshgrid(dz_lon_range, dz_lat_range)

      dz_min_lat = (np.abs(dz_xlat_full[:,0]-np.min(newse_lat))).argmin()
      dz_max_lat = (np.abs(dz_xlat_full[:,0]-np.max(newse_lat))).argmin()
      dz_min_lon = (np.abs(dz_xlon_full[0,:]-np.min(newse_lon))).argmin()
      dz_max_lon = (np.abs(dz_xlon_full[0,:]-np.max(newse_lon))).argmin()

      dz_xlat = dz_xlat_full[dz_min_lat:dz_max_lat,dz_min_lon:dz_max_lon]
      dz_xlon = dz_xlon_full[dz_min_lat:dz_max_lat,dz_min_lon:dz_max_lon]

      dz_sw_xlat = dz_xlat[0,0]
      dz_sw_xlon = dz_xlon[0,0]
      dz_ne_xlat = dz_xlat[-1,-1]
      dz_ne_xlon = dz_xlon[-1,-1]

      dz_map = Basemap(llcrnrlon=dz_sw_xlon, llcrnrlat=dz_sw_xlat, urcrnrlon=dz_ne_xlon, urcrnrlat=dz_ne_xlat, projection='lcc', lat_1=true_lat1, lat_2=true_lat2, lat_0=cen_lat, lon_0=cen_lon, resolution = resolution, area_thresh = area_thresh)

      dz_x_offset, dz_y_offset = dz_map(cen_lon, cen_lat)
      dz_x, dz_y = map(dz_xlon[:], dz_xlat[:])

      dz_x = dz_x - dz_x_offset
      dz_y = dz_y - dz_y_offset

      dz_min_x, dz_min_y = dz_map(dz_lon_range[dz_min_lon], dz_lat_range[dz_min_lat])
      dz_max_x, dz_max_y = dz_map(dz_lon_range[dz_max_lon], dz_lat_range[dz_max_lat])

      dz_min_x = dz_min_x - dz_x_offset
      dz_max_x = dz_max_x - dz_x_offset
      dz_min_y = dz_min_y - dz_y_offset
      dz_max_y = dz_max_y - dz_y_offset

################### Initialize interpolated variables and output .nc file: ############################

      var_cress = np.zeros((len(times),newse_lat.shape[0],newse_lat.shape[1]))
      var_cress_conv = np.zeros((len(times),newse_lat.shape[0],newse_lat.shape[1]))

      try:
         fout = netCDF4.Dataset(out_path, "w")
      except:
         print "Could not create %s!\n" % out_path

      fout.createDimension('NX', newse_lat.shape[1])
      fout.createDimension('NY', newse_lat.shape[0])
      fout.createDimension('NT', len(times))

      fout.createVariable('TIME', 'f4', ('NT',))
      fout.createVariable('XLAT', 'f4', ('NY','NX',))
      fout.createVariable('XLON', 'f4', ('NY','NX',))
      fout.createVariable('RADMASK', 'f4', ('NY','NX',))
      fout.createVariable('VAR_CRESSMAN', 'f4', ('NT','NY','NX',))
      fout.createVariable('VAR_CRESS_WINDOW', 'f4', ('NT','NY','NX',))
      fout.createVariable('VAR_CRESS_WINDOW_QC', 'f4', ('NT','NY','NX',))
      fout.createVariable('VAR_MAXFILTER', 'f4', ('NT','NY','NX',))
      fout.createVariable('VAR_CONVOLVE', 'f4', ('NT','NY','NX',))
      fout.createVariable('VAR_CONV_WINDOW', 'f4', ('NT','NY','NX',))
      fout.createVariable('VAR_CONV_WINDOW_QC', 'f4', ('NT','NY','NX',))

      fout.variables['XLAT'][:] = newse_lat
      fout.variables['XLON'][:] = newse_lon
      fout.variables['RADMASK'][:] = rad_mask
   else:
      try:
         fout = netCDF4.Dataset(out_path, "a")
      except:
         print "Could not create %s!\n" % out_path

################### Read MRMS Az. shear data, remove data outside NEWS-e domain for speed, and flip lat coordinates to be compatable: ###############
###################### Control added for WDSSii files (Az. Shear only) that use 'LatLonGrid' instead of 'SparseLatLonGrid' (11/2016) ########################

   grid_type = fin.DataType

   if (grid_type[0:2] == 'Sp'): #If SparseLatLonGrid 
      pixel = len(fin.dimensions["pixel"])
      dz_pixel = len(dzin.dimensions["pixel"])
      if (pixel > 0):
         pixel_x_full = fin.variables["pixel_x"][:]
         pixel_y_full = fin.variables["pixel_y"][:]
         pixel_value_full = fin.variables[var][:]
         pixel_count_full = fin.variables["pixel_count"][:]

         pixel_x_transpose = xlat_full.shape[0] - pixel_x_full

#         print 'pixel x stuff ... ', xlat_full.shape, np.min(pixel_x_full), np.max(pixel_x_full)
#         print np.min(pixel_x_transpose), np.max(pixel_x_transpose)
#         print 'index limits ... ', min_lat, max_lat, min_lon, max_lon
#         print np.min(pixel_y_full), np.max(pixel_y_full)

         pixel_x = pixel_x_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]
         pixel_y = pixel_y_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]
         pixel_value = pixel_value_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]
         pixel_count = pixel_count_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]

         print 'pixel count orig: ', len(pixel_value)
  
         pixel_value_temp = pixel_value[(pixel_value > 0.001) & (pixel_count > 1)]
         pixel_x_temp = pixel_x[(pixel_value > 0.001) & (pixel_count > 1)]
         pixel_y_temp = pixel_y[(pixel_value > 0.001) & (pixel_count > 1)]
         pixel_count_temp = pixel_count[(pixel_value > 0.001) & (pixel_count > 1)]

         for i in range(0, len(pixel_count_temp)):
            for j in range(1, pixel_count_temp[i]):
               temp_y_index = pixel_y_temp[i] + j
               temp_x_index = pixel_x_temp[i]            
               if (temp_y_index < max_lon):
                  pixel_x = np.append(pixel_x, temp_x_index)
                  pixel_y = np.append(pixel_y, temp_y_index)
                  pixel_value = np.append(pixel_value, pixel_value_temp[i])

         print 'pixel count new: ', len(pixel_value), len(pixel_count_temp)

         pixel_x = abs(pixel_x - (xlat_full.shape[0] - min_lat)) #... tortured way of flipping lat values, but it works
         pixel_y = pixel_y - min_lon
         pixel_x_val = x[pixel_x, pixel_y]  
         pixel_y_val = y[pixel_x, pixel_y]
 
         mrms_x_y = np.dstack([pixel_y_val, pixel_x_val])[0] #KD Tree searchable index of mrms observations

   elif (grid_type[0:2] == 'La'): #if LatLonGrid
      pixel_value_full = fin.variables[var][:]
      print 'shapes ... ', pixel_value_full.shape, xlon_indices.shape, xlat_indices.shape 

      pixel_value_full = pixel_value_full.ravel()
      pixel_x_full = xlat_indices.ravel()
      pixel_y_full = xlon_indices.ravel()
      pixel_x_transpose = pixel_x_full #xlat_full.shape[0] - pixel_x_full

#      print 'pixel x stuff ... ', xlat_indices[:,0], xlat_full.shape
#      print np.min(pixel_x_transpose), np.max(pixel_x_transpose)
#      print 'index limits ... ', min_lat, max_lat, min_lon, max_lon
#      print np.min(pixel_y_full), np.max(pixel_y_full)

      pixel_x = pixel_x_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]
      pixel_y = pixel_y_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]
      pixel_value = pixel_value_full[(pixel_x_transpose > min_lat) & (pixel_x_transpose < max_lat) & (pixel_y_full > min_lon) & (pixel_y_full < max_lon) & (pixel_value_full > var_thresh)]

      pixel_x = abs(pixel_x - (xlat_full.shape[0] - min_lat)) #... tortured way of flipping lat values, but it works
      pixel_y = pixel_y - min_lon
      pixel_x_val = x_full[pixel_x, pixel_y]
      pixel_y_val = y_full[pixel_x, pixel_y]

      mrms_x_y = np.dstack([pixel_y_val, pixel_x_val])[0] #KD Tree searchable index of mrms observations

   else: 
      print 'unknown grid type!!!!'

################### Read DZ data, remove data outside NEWS-e domain for speed, and flip lat coordinates to be compatable: ###############
######### Done separately for DZ and Az. shear since they are on different grids for 2016 data ############## 

   if (pixel > 0):
      dz_pixel_x_full = dzin.variables["pixel_x"][:]
      dz_pixel_y_full = dzin.variables["pixel_y"][:]
      dz_pixel_value_full = dzin.variables["MergedReflectivityQCComposite"][:]

      dz_pixel_count_full = dzin.variables["pixel_count"][:]

      dz_pixel_x_transpose = dz_xlat_full.shape[0] - dz_pixel_x_full

      dz_pixel_x = dz_pixel_x_full[(dz_pixel_x_transpose > dz_min_lat) & (dz_pixel_x_transpose < dz_max_lat) & (dz_pixel_y_full > dz_min_lon) & (dz_pixel_y_full < dz_max_lon) & (dz_pixel_value_full > var_thresh)]
      dz_pixel_y = dz_pixel_y_full[(dz_pixel_x_transpose > dz_min_lat) & (dz_pixel_x_transpose < dz_max_lat) & (dz_pixel_y_full > dz_min_lon) & (dz_pixel_y_full < dz_max_lon) & (dz_pixel_value_full > var_thresh)]
      dz_pixel_value = dz_pixel_value_full[(dz_pixel_x_transpose > dz_min_lat) & (dz_pixel_x_transpose < dz_max_lat) & (dz_pixel_y_full > dz_min_lon) & (dz_pixel_y_full < dz_max_lon) & (dz_pixel_value_full > var_thresh)]
      dz_pixel_count = dz_pixel_count_full[(dz_pixel_x_transpose > dz_min_lat) & (dz_pixel_x_transpose < dz_max_lat) & (dz_pixel_y_full > dz_min_lon) & (dz_pixel_y_full < dz_max_lon) & (dz_pixel_value_full > var_thresh)]

      print 'dz_pixel count orig: ', len(dz_pixel_value)

      dz_pixel_value_temp = dz_pixel_value[(dz_pixel_value > 40.) & (dz_pixel_count > 1)]
      dz_pixel_x_temp = dz_pixel_x[(dz_pixel_value > 40.) & (dz_pixel_count > 1)]
      dz_pixel_y_temp = dz_pixel_y[(dz_pixel_value > 40.) & (dz_pixel_count > 1)]
      dz_pixel_count_temp = dz_pixel_count[(dz_pixel_value > 40.) & (dz_pixel_count > 1)]

      for i in range(0, len(dz_pixel_count_temp)):
         for j in range(1, dz_pixel_count_temp[i]):
            dz_temp_y_index = dz_pixel_y_temp[i] + j
            dz_temp_x_index = dz_pixel_x_temp[i]
            if (dz_temp_y_index < dz_max_lon):
               dz_pixel_x = np.append(dz_pixel_x, dz_temp_x_index)
               dz_pixel_y = np.append(dz_pixel_y, dz_temp_y_index)
               dz_pixel_value = np.append(dz_pixel_value, dz_pixel_value_temp[i])

      print 'dz_pixel count new: ', len(dz_pixel_value), len(dz_pixel_count_temp)

      dz_pixel_x = abs(dz_pixel_x - (dz_xlat_full.shape[0] - dz_min_lat)) #... tortured way of flipping lat values, but it works
      dz_pixel_y = dz_pixel_y - dz_min_lon
      dz_pixel_x_val = dz_x[dz_pixel_x, dz_pixel_y]
      dz_pixel_y_val = dz_y[dz_pixel_x, dz_pixel_y]

      dz_mrms_x_y = np.dstack([dz_pixel_y_val, dz_pixel_x_val])[0] #KD Tree searchable index of mrms observations

################### If mrms obs are available within 5 min of time bin, Cressman them: ###############

      if ((len(mrms_x_y) > 0) and (len(dz_mrms_x_y) > 0) and (t_diff < 300.)):
         mrms_tree = scipy.spatial.cKDTree(mrms_x_y) 
         dz_mrms_tree = scipy.spatial.cKDTree(dz_mrms_x_y)

################### Find Az. Shear/DZ points to be interpolated: ###############

         oban_points = newse_tree.query_ball_tree(mrms_tree, roi)
         dz_points = newse_tree.query_ball_tree(dz_mrms_tree, dz_rad)
         print len(oban_points), 'oban points'
         print len(dz_points), 'dz points'

         oban_var = newse_x_ravel * 0.

################### Interpolate Az. Shear observations applying a proximity threshold to DZ: ###############

         for i in range(0, len(oban_points)):
            if (len(dz_points[i]) > 0.):
               dz_max_temp = np.max(dz_pixel_value[dz_points[i]])
            else:
               dz_max_temp = 0. 
            if (dz_max_temp > dz_thresh): 
               if ((len(oban_points[i]) > min_obs) and (len(near_rad_points[i]) == 0.) and (len(far_rad_points[i]) > 0.)):
                  dis = np.sqrt((pixel_x_val[oban_points[i]] - newse_x_ravel[i])**2 + (pixel_y_val[oban_points[i]] - newse_y_ravel[i])**2)         
                  weight = (roi**2 - dis**2) / (roi**2 + dis**2)
                  oban_var[i] = np.sum(weight * pixel_value[oban_points[i]]) / np.sum(weight)

         oban_var = oban_var.reshape(newse_x.shape[0], newse_x.shape[1])
  
################### Create maxfilter and convolved maxfilter versions of Az. shear data: ###############

         oban_var_maxfilter = get_local_maxima2d(oban_var, radius_max)
         oban_var_convolve = signal.convolve2d(oban_var_maxfilter, kernel, 'same')

################### Write Cressman interpolated data to .nc file: ###############

         fout.variables['TIME'][t] = temp_time
         fout.variables['VAR_CRESSMAN'][t,:,:] = oban_var
         fout.variables['VAR_MAXFILTER'][t,:,:] = oban_var_maxfilter
         fout.variables['VAR_CONVOLVE'][t,:,:] = oban_var_convolve
      else: ##fill with zeros for missing mrms times
         print 'missing! ', t_diff, len(dz_mrms_x_y) 
         oban_var = newse_x_ravel * 0.
         oban_var = oban_var.reshape(newse_x.shape[0], newse_x.shape[1])
         oban_var_convolve = oban_var
         fout.variables['TIME'][t] = times[t]
         fout.variables['VAR_CRESSMAN'][t,:,:] = oban_var
         fout.variables['VAR_MAXFILTER'][t,:,:] = oban_var
         fout.variables['VAR_CONVOLVE'][t,:,:] = oban_var

      var_cress[t,:,:] = oban_var
      var_cress_conv[t,:,:] = oban_var_convolve

      fin.close()
      del fin
      fout.close()
      del fout

   else: 
      fout.variables['TIME'][t] = temp_time
      fin.close()
      del fin
      fout.close()
      del fout

################### Create Rotation tracks of interpolated Az. Shear data: ###############
################### Apply size and continuity thresholds to rotation tracks: ###############

try:
   fout = netCDF4.Dataset(out_path, "a")
except:
   print "Could not create %s!\n" % out_path

cress_window = np.zeros((len(times),newse_lat.shape[0],newse_lat.shape[1]))
cress_conv_window = np.zeros((len(times),newse_lat.shape[0],newse_lat.shape[1]))

cress_window_qc = np.zeros((len(times),newse_lat.shape[0],newse_lat.shape[1]))
cress_conv_window_qc = np.zeros((len(times),newse_lat.shape[0],newse_lat.shape[1]))

for t in range(0, len(times)): 
   if ((t >= time_window) and (t < (len(times)-time_window))):
      cress_window_temp = var_cress[(t-time_window):(t+time_window),:,:]
      cress_conv_window_temp = var_cress_conv[(t-time_window):(t+time_window),:,:]
      cress_window[t,:,:] = np.max(cress_window_temp, axis=0) 
      cress_window_indices = np.argmax(cress_window_temp, axis=0)
      cress_conv_window[t,:,:] = np.max(cress_conv_window_temp, axis=0) 

      fout.variables['VAR_CRESS_WINDOW'][t,:,:] = cress_window[t,:,:]
      fout.variables['VAR_CONV_WINDOW'][t,:,:] = cress_conv_window[t,:,:]

      cress_init = np.where(cress_window[t,:,:] >= aws_thresh, cress_window[t,:,:], 0.)
      cress_window_qc_temp = cress_init * 0.
      cress_window_int = np.where(cress_window[t,:,:] >= aws_thresh, 1, 0)
      cress_labels = skimage.measure.label(cress_window_int)
      cress_labels = cress_labels.astype(int)

      cress_props = regionprops(cress_labels, cress_init)

      qc_obs = []
      for i in range(0, len(cress_props)):
         if ((cress_props[i].area > area_thresh2)): # and (cress_props[i].eccentricity > eccent_thresh)):
            qc_obs.append(i)

      for i in range(0, len(qc_obs)):
         temp_object = np.where(cress_labels == (qc_obs[i]+1), cress_window_indices, 0.)
         temp_num_times = np.unique(temp_object)
         print 'asdfasdf', np.min(temp_object), np.max(temp_object), len(temp_num_times), temp_num_times
         if (len(temp_num_times) > 2): #require swath object to contain values from at least 2 different times 
            cress_window_qc_temp = np.where(cress_labels == (qc_obs[i]+1), cress_init, cress_window_qc_temp)

      cress_window_qc[t,:,:] = cress_window_qc_temp

      cress_maxfilter_window = get_local_maxima2d(cress_window_qc_temp, radius_max)
      cress_conv_window_qc[t,:,:] = signal.convolve2d(cress_maxfilter_window, kernel, 'same')
      
################### Write Rotation track object fields to .nc file: ###############

      fout.variables['VAR_CRESS_WINDOW_QC'][t,:,:] = cress_window_qc[t,:,:]
      fout.variables['VAR_CONV_WINDOW_QC'][t,:,:] = cress_conv_window_qc[t,:,:]

fout.close()
del fout

