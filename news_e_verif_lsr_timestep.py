#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.basemap import Basemap
from scipy import signal
from scipy import *
from scipy import ndimage
import skimage
from skimage.morphology import label
from skimage.measure import regionprops
import math
from math import radians, tan, sin, cos, pi, atan, sqrt, pow, asin, acos
import pylab as P
import numpy as np
from numpy import NAN
import sys
import netCDF4
from optparse import OptionParser
from netcdftime import utime
import os
import time as timeit
import ctables
import news_e_post_cbook
from news_e_post_cbook import *
import news_e_plotting_cbook_v2
from news_e_plotting_cbook_v2 import *

sys.path.append("/scratch/software/anaconda/bin")

####################################### File Variables: ######################################################

parser = OptionParser()
parser.add_option("-d", dest="exp_dir", type="string", default= None, help="Input Directory (of summary files)")
parser.add_option("-o", dest="outdir", type="string", help = "Output Directory (for images)")
parser.add_option("-w", dest="warn_path", type="string", help = "Path to Shapefile of Warning Products")
parser.add_option("-l", dest="lsr_path", type="string", help = "Path to Shapefile of LSR Products")
parser.add_option("-t", dest="t", type="int", help = "Timestep to process")

(options, args) = parser.parse_args()

if ((options.exp_dir == None) or (options.outdir == None) or (options.warn_path == None) or (options.lsr_path == None) or (options.t == None)):
   print
   parser.print_help()
   print
   sys.exit(1)
else:
   exp_dir = options.exp_dir
   outdir = options.outdir
   warn_path = options.warn_path
   lsr_path = options.lsr_path
   t = options.t

domain = 'full'

#################################### User-Defined Variables:  #####################################################

edge            = 7 		#number of grid points to remove from near domain boundaries
thin		= 6		#thinning factor for quiver values (e.g. 6 means slice every 6th grid point)

radius_max      = 3		#grid point radius for maximum value filter
radius_gauss	= 2		#grid point radius of convolution operator 
neighborhood 	= 15		#grid point radius of prob matched mean neighborhood

plot_alpha 	= 0.6		#transparency value for filled contour plots

#################################### Threshold Values:  #####################################################

az_thresh       = [0.0075, 0.01, 0.0125, 0.015, 0.0175, 0.02]
wz_thresh 	= [0.001, 0.002, 0.003, 0.004, 0.005, 0.006]		#vertical vorticity thresh (s^-1)
ws_thresh 	= [10., 15., 20., 25., 30., 35.]		#wind speed thresholds (m s^-1)
dz_thresh       = [35., 40., 45., 50., 55., 60.]		#radar reflectivity thresholds (dBZ)
graup_thresh    = [4., 8., 12., 16., 20., 24.]

perc            = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]		#Ens. percentiles to calc/plot

#################################### Define colormaps:  #####################################################

wz_cmap       = matplotlib.colors.ListedColormap([cb_colors.blue2, cb_colors.blue3, cb_colors.blue4, cb_colors.red2, cb_colors.red3, cb_colors.red4, cb_colors.red5, cb_colors.red6, cb_colors.red7])

dz_cmap       = matplotlib.colors.ListedColormap([cb_colors.blue3, cb_colors.blue4, cb_colors.green4, cb_colors.green5, cb_colors.orange4, cb_colors.orange5, cb_colors.orange6, cb_colors.red6, cb_colors.red7])

wind_cmap = matplotlib.colors.ListedColormap([cb_colors.gray1, cb_colors.gray2, cb_colors.gray3, cb_colors.orange2, cb_colors.orange3, cb_colors.orange4, cb_colors.orange5, cb_colors.orange6, cb_colors.orange7, cb_colors.orange8])

wz_cmap_extend = matplotlib.colors.ListedColormap([cb_colors.blue2, cb_colors.blue3, cb_colors.blue4, cb_colors.red2, cb_colors.red3, cb_colors.red4, cb_colors.red5, cb_colors.red6, cb_colors.red7, cb_colors.purple7, cb_colors.purple6, cb_colors.purple5, cb_colors.purple4])

cape_cmap = matplotlib.colors.ListedColormap([cb_colors.gray1, cb_colors.gray2, cb_colors.gray3, cb_colors.orange3, cb_colors.orange4, cb_colors.orange5, cb_colors.orange6, cb_colors.red7, cb_colors.red6, cb_colors.red5, cb_colors.red4, cb_colors.purple4, cb_colors.purple5, cb_colors.purple6, cb_colors.purple7])

#################################### Basemap Variables:  #####################################################

resolution 	= 'h'
area_thresh 	= 1000.

damage_files = '' #['/Volumes/fast_scr/pythonletkf/vortex_se/2013-11-17/shapefiles/extractDamage_11-17/extractDamagePaths']

#################################### Contour Levels:  #####################################################

prob_levels    		= np.arange(0.1,1.1,0.1)		#(%)
aws_levels              = np.arange(0.005,0.0325,0.0025)        #(s^-1)
wz_levels               = np.arange(0.002,0.01175,0.00075)      #(s^-1)
uh2to5_levels           = np.arange(40.,560.,40.)               #(m^2 s^-2)
uh0to2_levels           = np.arange(15.,210.,15.)               #(m^2 s^-2)
cape_levels             = np.arange(250.,4000.,250.)            #(J Kg^-1)
cin_levels              = np.arange(-325.,0.,25.)               #(J Kg^-1)
temp_levels             = np.arange(40., 103., 3.)              #(deg F)
td_levels               = np.arange(30., 80., 2.)               #(deg F)
ws_levels_low           = np.arange(5.,35.,3.)                  #(m s^-1)
ws_levels_high          = np.arange(15.,45.,3.)                 #(m s^-1)
srh_levels              = np.arange(30.,480.,30.)               #(m^2 s^-2)
stp_levels              = np.arange(0.25,7.75,0.5)              #(unitless)
rain_levels             = np.arange(0.0,2.25,0.15)              #(in)
dz_levels               = np.arange(5.0,75.,5.)                 #(dBZ)
dz_levels2              = np.arange(15.0,65.,5.)                #(dBZ)
wup_levels              = np.arange(3.,42.,3.)
graup_levels            = np.arange(3.,42.,3.)

pmm_dz_levels           = [35., 50.]                            #(dBZ) 
pmm_dz_colors_gray      = [cb_colors.gray8, cb_colors.gray8]    #gray contours
#pmm_dz_colors_blue      = [cb_colors.blue6, cb_colors.blue8]    #blue contours
#pmm_dz_colors_red       = [cb_colors.red6, cb_colors.red8]      #red contours
#pmm_dz_colors_green     = [cb_colors.green6, cb_colors.green8]  #red contours

aws_cont_levels         = [0.01, 0.02, 0.03, 0.04]      #(s^-1)
aws_cont_colors         = [cb_colors.gray4, cb_colors.gray5, cb_colors.gray6, cb_colors.gray7]

wz_cont_levels          = [0.002, 0.004, 0.006, 0.008]
wz_cont_colors          = [cb_colors.orange4, cb_colors.orange5, cb_colors.orange6, cb_colors.orange7]

#################################### Initialize plot attributes using 'web plot' objects:  #####################################################

wz_plot = web_plot('',                   \
                   '',			\
                   'Probability Matched Mean - Composite Reflectivity (dBZ)',                   \
                   cb_colors.gray6,      \
                   wz_levels,            \
                   pmm_dz_levels,        \
                   '',                   \
                   '',                   \
                   pmm_dz_colors_gray,        \
                   cb_colors.purple3,		\
                   'none',		\
                   wz_cmap_extend,              \
                   'max',               \
                   plot_alpha,                \
                   neighborhood)  

ws_plot = web_plot('',                   \
                   '',                    \
                   'Probability Matched Mean - Composite Reflectivity (dBZ)',                   \
                   cb_colors.gray6,      \
                   ws_levels_low,            \
                   pmm_dz_levels,        \
                   '',                   \
                   '',                   \
                   pmm_dz_colors_gray,        \
                   cb_colors.orange9,           \
                   'none',              \
                   wind_cmap,              \
                   'max',                \
                   plot_alpha,                \
                   neighborhood)

rain_plot = web_plot('',                 \
                   '',                   \
                   'Probability Matched Mean - Composite Reflectivity (dBZ)',                   \
                   cb_colors.gray6,      \
                   rain_levels,          \
                   pmm_dz_levels,        \
                   '',                   \
                   '',                   \
                   pmm_dz_colors_gray,        \
                   cb_colors.purple3,           \
                   'none',              \
                   wz_cmap_extend,              \
                   'max',                \
                   plot_alpha,               \
                   neighborhood)

prob_plot = web_plot('wz0to2problsr',                 \
                   'Probability of 0-2 km Vertical Vort. > 0.003 s$^{-1}$',                  \
                   'Probability Matched Mean - Composite Reflectivity (dBZ)',                   \
                   cb_colors.gray6,      \
                   prob_levels,          \
                   pmm_dz_levels,        \
                   '',                   \
                   '',                   \
                   pmm_dz_colors_gray,        \
                   'none',              \
                   'none',              \
                   wz_cmap,              \
                   'neither',            \
                   plot_alpha,               \
                   neighborhood)

######################################################################################################
#################################### Read Data:  #####################################################
######################################################################################################

################### Read probability matched mean composite reflectivity: ############################

pmm_file = os.path.join(exp_dir, 'pmm_dz.nc')
try:
   fin = netCDF4.Dataset(pmm_file, "r")
   print "Opening %s \n" % pmm_file
except:
   print "%s does not exist! \n" % pmm_file
   sys.exit(1)

pmm_dz = fin.variables['PMM_DZ'][:]
fin.close()
del fin

##################### Get list of summary files to process: ##############################

files = []
files_temp = os.listdir(exp_dir)
for f, file in enumerate(files_temp):
   if (file[0] == '2'):
      files.append(file)

files.sort()
ne = len(files)

############### for each ensemble member summary file: #############################

for f, file in enumerate(files):
   exp_file = os.path.join(exp_dir, file)

   try:
      fin = netCDF4.Dataset(exp_file, "r")
      print "Opening %s \n" % exp_file
   except:
      print "%s does not exist! \n" % exp_file
      sys.exit(1)

############## Get grid/forecast time information from first summary file: ##################

   if (f == 0):

############# Get/process date/time info, handle 00Z shift for day but not month/year ############

      date = file[0:10]
      init_date = date
      init_label = 'Init: ' + date + ', ' + file[11:13] + file[14:16] + ' UTC'
      init_hr = int(file[11:13])

      time_full = fin.variables['TIME'][:]
      time = fin.variables['TIME'][t]
#      if (time is ma.masked):
#         time = fin.variables['TIME'][t-1] + 300.

      if (time < 25000.):
         time_correct = time + 86400.
      else:
         time_correct = time

      valid_hour = np.floor(time / 3600.)
      valid_min = np.floor((time - valid_hour * 3600.) / 60.)

      if (valid_hour > 23):
         valid_hour = valid_hour - 24
         if (init_hr > 20):
            temp_day = int(date[-2:])+1
            temp_day = str(temp_day)
            if (len(temp_day) == 1):
               temp_day = '0' + temp_day
            date = date[:-2] + temp_day

      valid_hour = str(int(valid_hour))
      valid_min = str(int(valid_min))

      if (len(valid_hour) == 1):
         valid_hour = '0' + valid_hour
      if (len(valid_min) == 1):
         valid_min = '0' + valid_min

      valid_label = 'Valid: ' + date + ', ' + valid_hour + valid_min + ' UTC'

############ Get grid/projection info, chop 'edge' from boundaries: ####################

      xlat = fin.variables['XLAT'][:]
      xlon = fin.variables['XLON'][:]
      xlat = xlat[edge:-edge,edge:-edge]
      xlon = xlon[edge:-edge,edge:-edge]

      sw_lat_full = xlat[0,0]
      sw_lon_full = xlon[0,0]
      ne_lat_full = xlat[-1,-1]
      ne_lon_full = xlon[-1,-1]

      cen_lat = fin.CEN_LAT
      cen_lon = fin.CEN_LON
      stand_lon = fin.STAND_LON
      true_lat1 = fin.TRUE_LAT1
      true_lat2 = fin.TRUE_LAT2

######################### Initialize variables: ####################################

      wz_0to2 = np.zeros((ne, len(time_full), xlat.shape[0], xlat.shape[1]))
      ws_10 = np.zeros((ne, len(time_full), xlat.shape[0], xlat.shape[1]))
      graup_max = np.zeros((ne, len(time_full), xlat.shape[0], xlat.shape[1]))
      dz = np.zeros((ne, len(time_full), xlat.shape[0], xlat.shape[1]))
      rain = np.zeros((ne, len(time_full), xlat.shape[0], xlat.shape[1]))

######################## Read in summary file variables: ################################

   wz_0to2[f,:,:] = fin.variables['WZ_0TO2'][:,edge:-edge,edge:-edge]
   ws_10[f,:,:] = fin.variables['WS_MAX_10'][:,edge:-edge,edge:-edge]
   graup_max[f,:,:] = fin.variables['GRAUPEL_MAX'][:,edge:-edge,edge:-edge]
   dz[f,:,:] = fin.variables['DZ_COMP'][:,edge:-edge,edge:-edge]
   rain[f,:,:] = fin.variables['RAIN'][:,edge:-edge,edge:-edge]

   fin.close()
   del fin

rain = np.where(rain <=0.01, -1, rain) #hack to not plot trace amounts of rain

############# Get/process Initial date/time info, handle 00Z shift for day but not month/year ############

start_hour = np.floor(time_full[0] / 3600.)
start_min = np.floor((time_full[0] - start_hour * 3600.) / 60.)

str_hour = str(int(start_hour))
str_min = str(int(start_min))

if (len(str_hour) == 1):
   str_hour = '0' + str_hour
if (len(str_min) == 1):
   str_min = '0' + str_min

##################### Calculate ensemble mean values: ###################################

mean_dz = np.mean(dz, axis=0)
mean_wz_0to2 = np.mean(wz_0to2, axis=0)
mean_ws_10 = np.mean(ws_10, axis=0)
mean_graup_max = np.mean(graup_max, axis=0)
mean_rain = np.mean(rain, axis=0)

#################################### Calc Swaths:  #####################################################

print 'swath part'

########################################################################################################
### If vertical vorticity, run maximum value and convolution filters
### over the raw data to spread and smooth the data
########################################################################################################

wz_0to2_convolve_temp = wz_0to2 * 0.
wz_0to2_convolve = wz_0to2 * 0.

kernel = gauss_kern(radius_gauss)						#convolution kernel for vertical vorticity

for n in range(0, wz_0to2.shape[0]):
   for tt in range(0, wz_0to2.shape[1]):
      wz_0to2_convolve_temp[n,tt,:,:] = get_local_maxima2d(wz_0to2[n,tt,:,:], radius_max)
      wz_0to2_convolve[n,tt,:,:] = signal.convolve2d(wz_0to2_convolve_temp[n,tt,:,:], kernel, 'same')

################################# Make Figure Template: ###################################################

print 'basemap part'

map, fig, ax1, ax2, ax3 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh)

x, y = map(xlon[:], xlat[:])
xx, yy = map.makegrid(xlat.shape[1], xlat.shape[0], returnxy=True)[2:4]   #equidistant x/y grid for streamline plots

###################################### Calc Percentiles: ###############################################

wz_0to2perc = np.zeros((len(perc), wz_0to2.shape[1], wz_0to2.shape[2], wz_0to2.shape[3]))
ws_10perc = np.zeros((len(perc), wz_0to2.shape[1], wz_0to2.shape[2], wz_0to2.shape[3]))
graup_maxperc = np.zeros((len(perc), wz_0to2.shape[1], wz_0to2.shape[2], wz_0to2.shape[3]))
rainperc = np.zeros((len(perc), wz_0to2.shape[1], wz_0to2.shape[2], wz_0to2.shape[3]))

for i in range(0, len(perc)):
   wz_0to2perc[i,:,:,:] = np.percentile(wz_0to2_convolve, perc[i], axis=0)
   ws_10perc[i,:,:,:] = np.percentile(ws_10, perc[i], axis=0)
   graup_maxperc[i,:,:,:] = np.percentile(graup_max, perc[i], axis=0)
   rainperc[i,:,:,:] = np.percentile(rain, perc[i], axis=0)

##################################### Calc Probabilities: #############################################

wz_0to2prob = np.zeros((len(wz_thresh), wz_0to2_convolve.shape[1], wz_0to2_convolve.shape[2], wz_0to2_convolve.shape[3]))

for i in range(0, len(wz_thresh)):
   wz_0to2prob[i,:,:,:] = calc_prob(wz_0to2_convolve, wz_thresh[i])

##################################### Calc swaths for current timestep: #############################################

if (t == 0): 
   wz_perc_temp = wz_0to2perc[:,0,:,:]
   wz_prob_temp = wz_0to2prob[:,0,:,:]
   ws_perc_temp = ws_10perc[:,0,:,:]
   graup_perc_temp = graup_maxperc[:,0,:,:]
   rain_perc_temp = rainperc[:,0,:,:]
elif (t == len(time_full)): 
   wz_perc_temp = np.max(wz_0to2perc, axis=1)
   wz_prob_temp = np.max(wz_0to2prob, axis=1)
   ws_perc_temp = np.max(ws_10perc, axis=1)
   graup_perc_temp = np.max(graup_maxperc, axis=1)
   rain_perc_temp = np.max(rainperc, axis=1)
else: 
   wz_perc_temp = np.max(wz_0to2perc[:,:(t+1),:,:], axis=1)
   wz_prob_temp = np.max(wz_0to2prob[:,:(t+1),:,:], axis=1)
   ws_perc_temp = np.max(ws_10perc[:,:(t+1),:,:], axis=1)
   graup_perc_temp = np.max(graup_maxperc[:,:(t+1),:,:], axis=1)
   rain_perc_temp = np.max(rainperc[:,:(t+1),:,:], axis=1)

##########################################################################################################
###################################### Make Plots: #######################################################
##########################################################################################################

print 'plot part'

##################################### Plot tornado LSRs w/vert vorticity swaths: #############################################

hail, wind, tornado = plot_lsr(map, fig, ax1, ax2, ax3, lsr_path, time_full[0], time_correct, plot_h='False', plot_w='False')

wz_plot.var1_levels = wz_levels

wz_plot.name = 'wz0to250lsr'
wz_plot.var1_title = 'Ens. 50th Percentile Value of 0-2 km Vertical Vort. (s$^{-1}$)'
env_plot(map, fig, ax1, ax2, ax3, x, y, wz_plot, wz_perc_temp[5,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

wz_plot.name = 'wz0to290lsr'
wz_plot.var1_title = 'Ens. 90th Percentile Value of 0-2 km Vertical Vort. (s$^{-1}$)'
env_plot(map, fig, ax1, ax2, ax3, x, y, wz_plot, wz_perc_temp[9,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

env_plot(map, fig, ax1, ax2, ax3, x, y, prob_plot, wz_prob_temp[2,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False')

##################################### Plot all LSRs w/graup/wind speed: #############################################

remove_lsr(hail, wind, tornado)
hail, wind, tornado = plot_lsr(map, fig, ax1, ax2, ax3, lsr_path, time_full[0], time_correct, plot_t='False', plot_w='False')
wz_plot.var1_levles = graup_levels

wz_plot.var1_levels = graup_levels

wz_plot.name = 'graupmax50lsr'
wz_plot.var1_title = 'Ens. 50th Percentile Value of Col. Integrated Graupel (Kg m$^{-2}$)'
env_plot(map, fig, ax1, ax2, ax3, x, y, wz_plot, graup_perc_temp[5,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

wz_plot.name = 'graupmax90lsr'
wz_plot.var1_title = 'Ens. 90th Percentile Value of Col. Integrated Graupel (Kg m$^{-2}$)'
env_plot(map, fig, ax1, ax2, ax3, x, y, wz_plot, graup_perc_temp[9,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

remove_lsr(hail, wind, tornado)
hail, wind, tornado = plot_lsr(map, fig, ax1, ax2, ax3, lsr_path, time_full[0], time_correct, plot_t='False', plot_h='False')

ws_plot.name = 'ws1050lsr'
ws_plot.var1_title = 'Ens. 50th Percentile Value of 10 m Max Wind Speed (m s$^{-1}$)'
env_plot(map, fig, ax1, ax2, ax3, x, y, ws_plot, ws_perc_temp[5,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

ws_plot.name = 'ws1090lsr'
ws_plot.var1_title = 'Ens. 90th Percentile Value of 10 m Max Wind Speed (m s$^{-1}$)'
env_plot(map, fig, ax1, ax2, ax3, x, y, ws_plot, ws_perc_temp[9,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

remove_lsr(hail, wind, tornado)

##################################### Plot FF warnings w/rainfall: #############################################

ff = plot_warn(map, fig, ax1, ax2, ax3, warn_path, time_correct, cb_colors.green6, cb_colors.red6, ff='True')

rain_plot.name = 'rain50lsr'
rain_plot.var1_title = 'Ens. 50th Percentile Value of Accumulated Rain (inches)'
env_plot(map, fig, ax1, ax2, ax3, x, y, rain_plot, rain_perc_temp[5,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

rain_plot.name = 'rain90lsr'
rain_plot.var1_title = 'Ens. 90th Percentile Value of Accumulated Rain (inches)'
env_plot(map, fig, ax1, ax2, ax3, x, y, rain_plot, rain_perc_temp[9,:,:], pmm_dz[t,:,:], t, init_label, valid_label, domain, outdir, '', '', '', 5, 0, spec='False', quiv='False', showmax='True')

remove_warn(ff)

