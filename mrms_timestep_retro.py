#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.basemap import Basemap
from scipy import signal
from scipy import *
from scipy import ndimage
import skimage
from skimage.morphology import label
from skimage.measure import regionprops
import math
from math import radians, tan, sin, cos, pi, atan, sqrt, pow, asin, acos
import pylab as P
import numpy as np
from numpy import NAN
import sys
import netCDF4
from optparse import OptionParser
from netcdftime import utime
import os
import time as timeit
import ctables
import news_e_post_cbook
from news_e_post_cbook import *
import news_e_plotting_cbook_v2
from news_e_plotting_cbook_v2 import *

sys.path.append("/scratch/software/anaconda/bin")

####################################### Parse Options: ######################################################

parser = OptionParser()
parser.add_option("-d", dest="exp_dir", type="string", default= None, help="Input Directory (of summary files)")
parser.add_option("-o", dest="outdir", type="string", help = "Output Directory (for images)")
parser.add_option("-w", dest="warn_path", type="string", help = "Path to Shapefile of Warning Products")
parser.add_option("-l", dest="lsr_path", type="string", help = "Path to Shapefile of LSR Products")
parser.add_option("-z", dest="verif_file_dz", type="string", default= None, help="Input DZ file for verification data")
parser.add_option("-a", dest="verif_file_aws", type="string", default= None, help="Input AWS file for verification data")
parser.add_option("-m", dest="verif_file_aws_ml", type="string", default= None, help="Input ML AWS file for verification data")
parser.add_option("-t", dest="t", type="int", help = "Timestep to process")

(options, args) = parser.parse_args()

if ((options.exp_dir == None) or (options.outdir == None) or (options.warn_path == None) or (options.lsr_path == None) or (options.verif_file_dz == None) or (options.verif_file_aws == None) or (options.verif_file_aws_ml == None) or (options.t == None)):
   print
   parser.print_help()
   print
   sys.exit(1)
else:
   exp_dir = options.exp_dir
   outdir = options.outdir
   warn_path = options.warn_path
   lsr_path = options.lsr_path
   verif_file_dz = options.verif_file_dz
   verif_file_aws = options.verif_file_aws
   verif_file_aws_ml = options.verif_file_aws_ml
   t = options.t

domain = 'full'

#################################### User-Defined Variables:  #####################################################

temp_outtime = str((t) * 5)
if (len(temp_outtime) == 1):
   outtime = '00' + temp_outtime
elif (len(temp_outtime) == 3):
   outtime = temp_outtime
else:
   outtime = '0' + temp_outtime

edge            = 7 		#number of grid points to remove from near domain boundaries
thin		= 6		#thinning factor for quiver values (e.g. 6 means slice every 6th grid point)

fcst_len 	= 5400. 	#length of forecast to consider (s)
radius_max      = 3		#grid point radius for maximum value filter
radius_gauss	= 2		#grid point radius of convolution operator 
neighborhood 	= 15		#grid point radius of prob matched mean neighborhood

aws_thresh      = 0.0001        #hacks to contour objects
wz_thresh       = 0.0001

plot_alpha 	= 0.5		#transparency value for filled contour plots

#################################### Define Colormaps:  #####################################################

wz_cmap       = matplotlib.colors.ListedColormap([cb_colors.blue2, cb_colors.blue3, cb_colors.blue4, cb_colors.red2, cb_colors.red3, cb_colors.red4, cb_colors.red5, cb_colors.red6, cb_colors.red7])

dz_cmap_new       = matplotlib.colors.ListedColormap([cb_colors.green5, cb_colors.green4, cb_colors.green3, cb_colors.orange2, cb_colors.orange4, cb_colors.orange6, cb_colors.red6, cb_colors.red4, cb_colors.purple3, cb_colors.purple5])

dz_cmap       = matplotlib.colors.ListedColormap([cb_colors.blue3, cb_colors.blue4, cb_colors.green4, cb_colors.green5, cb_colors.orange4, cb_colors.orange5, cb_colors.orange6, cb_colors.red6, cb_colors.red7])

wz_cmap_extend = matplotlib.colors.ListedColormap([cb_colors.blue2, cb_colors.blue3, cb_colors.blue4, cb_colors.red2, cb_colors.red3, cb_colors.red4, cb_colors.red5, cb_colors.red6, cb_colors.red7, cb_colors.purple7, cb_colors.purple6, cb_colors.purple5, cb_colors.purple4])

#################################### Basemap Variables:  #####################################################

resolution 	= 'h'
area_thresh 	= 1000.

damage_files = '' #['/Volumes/fast_scr/pythonletkf/vortex_se/2013-11-17/shapefiles/extractDamage_11-17/extractDamagePaths']

#################################### Contour Levels:  #####################################################

aws_colors              = [cb_colors.gray8, cb_colors.gray8]    	#gray contours

aws_levels             	= [0.004, 0.5]
wz_levels              	= np.arange(0.002,0.01175,0.00075)          	#(s^-1)
dz_levels2              = np.arange(20.0,75.,5.)                        #(dBZ)
dz_levels              	= np.arange(15.0,65.,5.)                        #(dBZ)

#################################### Initialize plot attributes using 'web plot' objects:  #####################################################

dz_plot = web_plot('mrms_lsr',                   \
                   'MRMS Composite Radar Reflectivity (dBZ)',                  \
                   'MRMS 0-2 km Az. Shear (30-min Window; s$^{-1}$)',                   \
                   cb_colors.gray6,      \
                   dz_levels2,            \
                   aws_levels,        \
                   '',                   \
                   '',                   \
                   aws_colors,        \
                   cb_colors.purple7,              \
                   'none',              \
                   dz_cmap_new,              \
                   'max',                \
                   plot_alpha,               \
                   neighborhood)

######################################################################################################
#################################### Read Data:  #####################################################
######################################################################################################

##################### Get list of summary files to process: ##############################

files = []
files_temp = os.listdir(exp_dir)
for f, file in enumerate(files_temp):
   if (file[0] == '2'):
      files.append(file)

files.sort()
ne = len(files)

############### for each ensemble member summary file: #############################

for f, file in enumerate(files):
   exp_file = os.path.join(exp_dir, file)

   try:
      fin = netCDF4.Dataset(exp_file, "r")
      print "Opening %s \n" % exp_file
   except:
      print "%s does not exist! \n" % exp_file
      sys.exit(1)

############## Get grid/forecast time information from first summary file: ##################

   if (f == 0):
      date = file[0:10]
      init_date = date
      init_label = 'Init: ' + date + ', ' + file[11:13] + file[14:16] + ' UTC'
      init_hr = int(file[11:13])

      init_time = fin.variables['TIME'][0]
      time = fin.variables['TIME'][t]

      if (time < 35000.):
         time_correct = time + 86400. 
      else: 
         time_correct = time 

      valid_hour = np.floor(time / 3600.)
      valid_min = np.floor((time - valid_hour * 3600.) / 60.)

      if (valid_hour > 23):
         valid_hour = valid_hour - 24
         if (init_hr > 20):
            temp_day = int(date[-2:])+1
            temp_day = str(temp_day)
            if (len(temp_day) == 1):
               temp_day = '0' + temp_day
            date = date[:-2] + temp_day

      valid_hour = str(int(valid_hour))
      valid_min = str(int(valid_min))

      if (len(valid_hour) == 1):
         valid_hour = '0' + valid_hour
      if (len(valid_min) == 1):
         valid_min = '0' + valid_min

      valid_label = 'Valid: ' + date + ', ' + valid_hour + valid_min + ' UTC'

############ Get grid/projection info, chop 'edge' from boundaries: ####################

      xlat = fin.variables['XLAT'][:]
      xlon = fin.variables['XLON'][:]
      xlat = xlat[edge:-edge,edge:-edge]
      xlon = xlon[edge:-edge,edge:-edge]

      sw_lat_full = xlat[0,0]
      sw_lon_full = xlon[0,0]
      ne_lat_full = xlat[-1,-1]
      ne_lon_full = xlon[-1,-1]

      cen_lat = fin.CEN_LAT
      cen_lon = fin.CEN_LON
      stand_lon = fin.STAND_LON
      true_lat1 = fin.TRUE_LAT1
      true_lat2 = fin.TRUE_LAT2

   fin.close()
   del fin

############ Get forecast initialization time: ####################

start_hour = np.floor(time / 3600.)
start_min = np.floor((time - start_hour * 3600.) / 60.)

str_hour = str(int(start_hour))
str_min = str(int(start_min))

if (len(str_hour) == 1):
   str_hour = '0' + str_hour
if (len(str_min) == 1):
   str_min = '0' + str_min

#################################### Read MRMS Data:  #####################################################

####### Low-level Az Shear (0 - 2 km) ########

try:
   aws_in = netCDF4.Dataset(verif_file_aws, "r")
   print "Opening %s \n" % verif_file_aws
except:
   print "%s does not exist! \n" % verif_file_aws
   sys.exit(1)

vlat = aws_in.variables['XLAT'][:]
vlon = aws_in.variables['XLON'][:]
vtimes = aws_in.variables['TIME'][:]

vt = (np.abs(time_correct - vtimes)).argmin()

radmask = aws_in.variables['RADMASK'][edge:-edge,edge:-edge]
aws_qc = aws_in.variables['VAR_CRESS_WINDOW_QC'][vt,edge:-edge,edge:-edge]

aws_in.close()
del aws_in

####### Midlevel Az Shear (2 - 5 km) ########

try:
   aws_ml_in = netCDF4.Dataset(verif_file_aws_ml, "r")
   print "Opening %s \n" % verif_file_aws_ml
except:
   print "%s does not exist! \n" % verif_file_aws_ml
   sys.exit(1)

vlat_ml = aws_ml_in.variables['XLAT'][:]
vlon_ml = aws_ml_in.variables['XLON'][:]
vtimes_ml = aws_ml_in.variables['TIME'][:]

vt_ml = (np.abs(time_correct - vtimes_ml)).argmin()

aws_qc_ml = aws_ml_in.variables['VAR_CRESS_WINDOW_QC'][vt_ml,edge:-edge,edge:-edge]

aws_ml_in.close()
del aws_ml_in

####### Reflectivity ########

try:
   dz_in = netCDF4.Dataset(verif_file_dz, "r")
   print "Opening %s \n" % verif_file_dz
except:
   print "%s does not exist! \n" % verif_file_dz
   sys.exit(1)

dztimes = dz_in.variables['TIME'][:]
dzt = (np.abs(time_correct - dztimes)).argmin()

dz_qc = dz_in.variables['VAR_CRESSMAN'][(dzt),edge:-edge,edge:-edge]

dz_in.close()
del dz_in

################################# Make Figure Template: ###################################################

print 'basemap part'

map, fig, ax1, ax2, ax3 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh)

x, y = map(xlon[:], xlat[:])
xx, yy = map.makegrid(xlat.shape[1], xlat.shape[0], returnxy=True)[2:4]   #equidistant x/y grid for streamline plots

vx, vy = map(vlon[:], vlat[:])

###################################### Make Plots: #######################################################

print 'plot part'

######## Plot NWS Warnings and LSRs: ########

hail, wind, tornado = plot_lsr(map, fig, ax1, ax2, ax3, lsr_path, init_time, time_correct)
svr, tor = plot_warn(map, fig, ax1, ax2, ax3, warn_path, time_correct, cb_colors.blue6, cb_colors.red6)

######## Plot composite reflectivity overlain with either 0-2 or 2-5 Az. shear rotation tracks: ########

mrms_plot(map, fig, ax1, ax2, ax3, x, y, dz_plot, dz_qc[:,:], aws_qc[:,:], t, init_label, valid_label, domain, outdir, outtime)

dz_plot.name = 'mrms_lsr_ml'
dz_plot.var2_title = 'MRMS 2-5 km Az. Shear (30-min Window; s$^{-1}$)'

mrms_plot(map, fig, ax1, ax2, ax3, x, y, dz_plot, dz_qc[:,:], aws_qc_ml[:,:], t, init_label, valid_label, domain, outdir, outtime)

remove_warn(svr)
remove_warn(tor)
remove_lsr(hail, wind, tornado)

