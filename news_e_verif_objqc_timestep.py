#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.basemap import Basemap
from scipy import signal
from scipy import *
from scipy import ndimage
import skimage
from skimage.morphology import label
from skimage.measure import regionprops
import math
from math import radians, tan, sin, cos, pi, atan, sqrt, pow, asin, acos
import pylab as P
import numpy as np
from numpy import NAN
import sys
import netCDF4
from optparse import OptionParser
from netcdftime import utime
import os
import time as timeit
import ctables
import news_e_post_cbook
from news_e_post_cbook import *
import news_e_plotting_cbook_v2
from news_e_plotting_cbook_v2 import *

####################################### File Variables: ######################################################

parser = OptionParser()
parser.add_option("-d", dest="exp_dir", type="string", default= None, help="Input Directory (of summary files)")
parser.add_option("-o", dest="outdir", type="string", help = "Output Directory (for images)")
parser.add_option("-w", dest="warn_path", type="string", help = "Path to Shapefile of Warning Products")
parser.add_option("-l", dest="lsr_path", type="string", help = "Path to Shapefile of LSR Products")
parser.add_option("-a", dest="verif_file", type="string", default= None, help="Input AWS file for verification data")
parser.add_option("-v", dest="var", type="string", default=None, help="Variable to process")
parser.add_option("-t", dest="t", type="int", help = "Timestep to process")

(options, args) = parser.parse_args()

if ((options.exp_dir == None) or (options.outdir == None) or (options.warn_path == None) or (options.lsr_path == None) or (options.verif_file == None) or (options.var == None) or (options.t == None)):
   print
   parser.print_help()
   print
   sys.exit(1)
else:
   exp_dir = options.exp_dir
   outdir = options.outdir
   warn_path = options.warn_path
   lsr_path = options.lsr_path
   verif_file = options.verif_file
   var = options.var
   t = options.t

domain = 'full'

#################################### User-Defined Variables:  #####################################################

edge            = 7 		#number of grid points to remove from near domain boundaries
thin		= 6		#thinning factor for quiver values (e.g. 6 means slice every 6th grid point)

radius_max      = 3		#grid point radius for maximum value filter
radius_gauss	= 2		#grid point radius of convolution operator 
neighborhood 	= 15		#grid point radius of prob matched mean neighborhood

plot_alpha 	= 0.5		#transparency value for filled contour plots

cutoff_rad = 30. 		#maximum distance between two objects to be matched in km
cutoff_time = 1200. 		#maximum time between two objects to be matched in s

score_thresh    = 0.1

obj_area_thresh     = 6.
aws_thresh      = 0.0001        #hacks to contour objects
wz_thresh       = 0.0001
dz_thresh       = 40. 

#################################### Colormap Names:  #####################################################

hit_cmap        = matplotlib.colors.ListedColormap([cb_colors.blue3, cb_colors.blue4, cb_colors.blue5, cb_colors.blue6, cb_colors.blue7])
fa_cmap        = matplotlib.colors.ListedColormap([cb_colors.orange3, cb_colors.orange4, cb_colors.orange5, cb_colors.orange6, cb_colors.orange7])

ti_cmap         = matplotlib.colors.ListedColormap([cb_colors.orange6, cb_colors.orange4, cb_colors.blue4, cb_colors.blue4, cb_colors.blue5, cb_colors.blue5, cb_colors.blue6, cb_colors.blue6, cb_colors.blue7, cb_colors.blue7])

#################################### Basemap Variables:  #####################################################

resolution      = 'h'
area_thresh     = 1000.

damage_files = '' #['/Volumes/fast_scr/pythonletkf/vortex_se/2013-11-17/shapefiles/extractDamage_11-17/extractDamagePaths']

#################################### Contour Levels:  #####################################################

prob_levels    		= np.arange(0.1,1.1,0.1)		#(%)
ob_prob_levels          = np.arange(0.05,0.55,0.1)               #(%)
wz_levels      	 	= np.arange(0.0015,0.00825,0.00075)	#(s^-1)
aws_levels              = np.arange(0.005,0.0325,0.0025)        #(s^-1)

ti_levels               = np.arange(0.,1.1,0.1)

#################################### Initialize plot attributes:  #####################################################

object_plot = ob_plot('',	\
                   '',                  \
                   '',                        \
                   cb_colors.blue6,      \
                   cb_colors.orange6,     \
                   ob_prob_levels,              \
                   ob_prob_levels,     \
                   hit_cmap,                      \
                   fa_cmap,                     \
                   '',  \
                   [cb_colors.gray7, cb_colors.gray7], \
                   'max', 	\
                   plot_alpha, 	\
                   neighborhood)

#################################### Assign variable specific attributes:  #####################################################

if (var == 'WZ'):
   object_plot.name = 'aws_wz0to2_obmatch'
   object_plot.var1_title = 'Prob. of Match [AzSh, Vert. Vort. (s$^{-1}$)]'
   object_plot.var2_title = 'Prob. of False Alarm [AzSh, Vert. Vort. (s$^{-1}$)]'
   object_plot.var3_level = [aws_thresh, 100.]
elif (var == 'UH0TO2'):
   object_plot.name = 'aws_uh0to2_obmatch'
   object_plot.var1_title = 'Prob. of Match [AzSh (s$^{-1}$), UH 0-2 km (m$^{2}$ s$^{-2}$)]'
   object_plot.var2_title = 'Prob. of False Alarm [AzSh (s$^{-1}$), UH 0-2 km (m$^{2}$ s$^{-2}$)]'
   object_plot.var3_level = [aws_thresh, 100.]
elif (var == 'UH2TO5'):
   object_plot.name = 'aws_uh2to5_obmatch'
   object_plot.var1_title = 'Prob. of Match [AzSh (s$^{-1}$), UH 2-5 km (m$^{2}$ s$^{-2}$)]'
   object_plot.var2_title = 'Prob. of False Alarm [AzSh (s$^{-1}$), UH 2-5 km (m$^{2}$ s$^{-2}$)]'
   object_plot.var3_level = [aws_thresh, 100.]
elif (var == 'DZ'):
   object_plot.name = 'dz_obmatch'
   object_plot.var1_title = 'Prob. of Match [Comp. Reflectivity (> 40 dBZ)]'
   object_plot.var2_title = 'Prob. of False Alarm [Comp. Reflecitivity (> 40 dBZ)]'
   object_plot.var3_level = [dz_thresh, 100.]
else: 
   print "%s variable not found" % var
   sys.exit(1)

######################################################################################################
#################################### Read Data:  #####################################################
######################################################################################################

##################### Get list of summary files to process: ##############################

files = []
files_temp = os.listdir(exp_dir)
for f, file in enumerate(files_temp):
   if (file[0] == '2'):
      files.append(file)

files.sort()
ne = len(files)

############### for each ensemble member summary file: #############################

for f, file in enumerate(files):
   exp_file = os.path.join(exp_dir, file)

   try:
      fin = netCDF4.Dataset(exp_file, "r")
      print "Opening %s \n" % exp_file
   except:
      print "%s does not exist! \n" % exp_file
      sys.exit(1)

############## Get grid/forecast time information from first summary file: ##################

   if (f == 0):

############# Get/process date/time info, handle 00Z shift for day but not month/year ############

      date = file[0:10]
      init_date = date
      init_label = 'Init: ' + date + ', ' + file[11:13] + file[14:16] + ' UTC'
      init_hr = int(file[11:13])

      init_time = fin.variables['TIME'][0]

      time = fin.variables['TIME'][:]
      plot_time = time[t]

      time = np.where(time < 35000., time+86400., time)
      if (time is ma.masked):
         time = fin.variables['TIME'][t-1] + 300.
      
      if (plot_time < 35000.):
         time_correct = plot_time + 86400.
      else:
         time_correct = plot_time

      valid_hour = np.floor(plot_time / 3600.)
      valid_min = np.floor((plot_time - valid_hour * 3600.) / 60.)

      if (valid_hour > 23):
         valid_hour = valid_hour - 24
         if (init_hr > 20):
            temp_day = int(date[-2:])+1
            temp_day = str(temp_day)
            if (len(temp_day) == 1):
               temp_day = '0' + temp_day
            date = date[:-2] + temp_day

      valid_hour = str(int(valid_hour))
      valid_min = str(int(valid_min))

      if (len(valid_hour) == 1):
         valid_hour = '0' + valid_hour
      if (len(valid_min) == 1):
         valid_min = '0' + valid_min

      valid_label = 'Valid: ' + date + ', ' + valid_hour + valid_min + ' UTC'

############ Get grid/projection info, chop 'edge' from boundaries: ####################

      xlat = fin.variables['XLAT'][:]
      xlon = fin.variables['XLON'][:]
      xlat = xlat[edge:-edge,edge:-edge]
      xlon = xlon[edge:-edge,edge:-edge]

      sw_lat_full = xlat[0,0]
      sw_lon_full = xlon[0,0]
      ne_lat_full = xlat[-1,-1]
      ne_lon_full = xlon[-1,-1]

      cen_lat = fin.CEN_LAT
      cen_lon = fin.CEN_LON
      stand_lon = fin.STAND_LON
      true_lat1 = fin.TRUE_LAT1
      true_lat2 = fin.TRUE_LAT2

######################### Initialize variables: ####################################

      dz = np.zeros((ne, len(time), xlat.shape[0], xlat.shape[1]))

######################## Read in summary file variables: ################################

   dz[f,:,:,:] = fin.variables['DZ_COMP'][:,edge:-edge,edge:-edge]

   fin.close()
   del fin

#dz = np.where(dz >= dz_thresh, dz, 0.) 

######################## Get/process Initial date/time info ###########################

start_hour = np.floor(time[0] / 3600.)
start_min = np.floor((time[0] - start_hour * 3600.) / 60.)

str_hour = str(int(start_hour))
str_min = str(int(start_min))

if (len(str_hour) == 1):
   str_hour = '0' + str_hour
if (len(str_min) == 1):
   str_min = '0' + str_min

############# Get/process forecast rotation object info - rot_file name is hard coded ############

rot_file = os.path.join(exp_dir, 'rot_qc_final.nc')
#rot_file = os.path.join(exp_dir, 'rot_qc.nc')

try:
   rot_in = netCDF4.Dataset(rot_file, "r")
   print "Opening %s \n" % rot_file
except:
   print "%s does not exist! \n" % rot_file
   sys.exit(1)

if (var == 'WZ'):
   fcst_var = rot_in.variables['WZ_0TO2_WINDOW_MASK'][:,:,edge:-edge,edge:-edge]
elif (var == 'UH0TO2'):
   fcst_var = rot_in.variables['UH_0TO2_WINDOW_MASK'][:,:,edge:-edge,edge:-edge]
elif (var == 'UH2TO5'):
   fcst_var = rot_in.variables['UH_2TO5_WINDOW_MASK'][:,:,edge:-edge,edge:-edge]
elif (var == 'DZ'):
   fcst_var = dz

rot_in.close()
del rot_in

#################################### Read Verif Data:  #####################################################

try:
   v_in = netCDF4.Dataset(verif_file, "r")
   print "Opening %s \n" % verif_file
except:
   print "%s does not exist! \n" % verif_file
   sys.exit(1)

if (var == 'DZ'):
   verif_var = v_in.variables['VAR_CRESSMAN'][:,edge:-edge,edge:-edge]
   var_thresh = dz_thresh
   radmask = verif_var[0,:,:] * 0.
else:
   verif_var = v_in.variables['VAR_CRESS_WINDOW_QC'][:,edge:-edge,edge:-edge]
   var_thresh = aws_thresh
   radmask = v_in.variables['RADMASK'][edge:-edge,edge:-edge]

vlat = v_in.variables['XLAT'][edge:-edge,edge:-edge]
vlon = v_in.variables['XLON'][edge:-edge,edge:-edge]
vtimes = v_in.variables['TIME'][:]

vt = (np.abs(time_correct - vtimes)).argmin()

v_in.close()
del v_in

################################# Make Figure Template: ###################################################

print 'basemap part'

map, fig, ax1, ax2, ax3 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh, object='True')

x, y = map(xlon[:], xlat[:])
xx, yy = map.makegrid(xlat.shape[1], xlat.shape[0], returnxy=True)[2:4]   #equidistant x/y grid for streamline plots

vx, vy = map(vlon[:], vlat[:])

###################################### Find Forecast ('f') and Verification ('v') Objects: #######################################################

print 'object verification part'

v_labels = verif_var * 0
f_labels = fcst_var * 0

for tt in range(0, len(vtimes)):
   v_objects_temp = np.where(verif_var[tt,:,:] >= var_thresh, 1, 0)
   v_labels[tt,:,:] = skimage.measure.label(v_objects_temp)

v_labels = v_labels.astype(int)

f_swath_labels = np.zeros((ne,x.shape[0],x.shape[1]))
f_swath_labels = f_swath_labels.astype(int)

for n in range(0, ne):
   f_swath_objects_temp = fcst_var[n,:,:,:]
   f_swath = np.max(f_swath_objects_temp, axis=0)
   f_swath_temp = np.where(f_swath>0.,1,0)
   f_swath_labels[n,:,:] = skimage.measure.label(f_swath_temp)

for tt in range(0, len(time)):
   for n in range(0, ne):
      f_objects_temp = np.where(fcst_var[n,tt,:,:] >= var_thresh, 1, 0)
      f_labels[n,tt,:,:] = skimage.measure.label(f_objects_temp)

f_labels = f_labels.astype(int)

f_ob_max = np.max(f_labels)
v_ob_max = np.max(v_labels)

print 'ob max: ', f_ob_max, v_ob_max

if (f_ob_max >= v_ob_max):
   ob_max = f_ob_max
else:
   ob_max = v_ob_max

###################################### Calc Verification Object Props: #######################################################

v_cent_x = np.zeros((len(vtimes),ob_max))
v_cent_y = np.zeros((len(vtimes),ob_max))
v_area = np.zeros((len(vtimes),ob_max))
v_max_intensity = np.zeros((len(vtimes),ob_max))
v_time = np.zeros((len(vtimes),ob_max))
v_index = np.zeros((len(vtimes),ob_max))

for tt in range(0, len(vtimes)):
   v_props = regionprops(v_labels[tt,:,:], verif_var[tt,:,:])
   if (var == 'DZ'): ###Enforce area threshold for DZ (already done for rotation)
      for i in range(0, len(v_props)):
         if (v_props[i].area > obj_area_thresh):
            cx_index = v_props[i].centroid[1]
            cy_index = v_props[i].centroid[0]
            v_cent_x[tt,i] = gridpoint_interp(vx, cx_index, cy_index)
            v_cent_y[tt,i] = gridpoint_interp(vy, cx_index, cy_index)
            v_area[tt,i] = v_props[i].area
            v_max_intensity[tt,i] = v_props[i].max_intensity
            v_time[tt,i] = vtimes[tt]
   else:
      for i in range(0, len(v_props)):
         cx_index = v_props[i].centroid[1]
         cy_index = v_props[i].centroid[0]
         v_cent_x[tt,i] = gridpoint_interp(vx, cx_index, cy_index)
         v_cent_y[tt,i] = gridpoint_interp(vy, cx_index, cy_index)
         v_area[tt,i] = v_props[i].area
         v_max_intensity[tt,i] = v_props[i].max_intensity
         v_time[tt,i] = vtimes[tt]

#################### Create DZ Verif Field with only objects > obj_area_thresh for plots: ##################################

if (var == 'DZ'): ###Enforce area threshold for DZ (already done for rotation)
   dz_plot_labels = []
   temp_object = verif_var[vt,:,:] * 0.

   v_props = regionprops(v_labels[vt,:,:], verif_var[vt,:,:])
   for i in range(0, len(v_props)):
     if (v_props[i].area > obj_area_thresh):
        dz_plot_labels.append(i)

   for i in range(0, len(dz_plot_labels)): 
      temp_object = np.where(v_labels[vt,:,:] == (dz_plot_labels[i]+1), 1., temp_object)

   plot_dz = np.where(temp_object > 0., verif_var[vt,:,:], 0.)

###################################### Calc Forecast Object Props: #######################################################

f_cent_x = np.zeros((len(time),ne,ob_max))
f_cent_y = np.zeros((len(time),ne,ob_max))
f_area = np.zeros((len(time),ne,ob_max))
f_max_intensity = np.zeros((len(time),ne,ob_max))
f_time = np.zeros((len(time),ne,ob_max))
f_index = np.zeros((len(time),ne,ob_max))

for tt in range(0, len(time)):
   for n in range(0, ne):
      f_props = regionprops(f_labels[n,tt,:,:], fcst_var[n,tt,:,:])
      if (var == 'DZ'): ###Enforce area threshold for DZ (already done for rotation) 
         for i in range(0, len(f_props)):
            if (f_props[i].area > obj_area_thresh):
               cx_index = f_props[i].centroid[1]
               cy_index = f_props[i].centroid[0]
               f_cent_x[tt,n,i] = gridpoint_interp(x, cx_index, cy_index)
               f_cent_y[tt,n,i] = gridpoint_interp(y, cx_index, cy_index)
               f_area[tt,n,i] = f_props[i].area
               f_max_intensity[tt,n,i] = f_props[i].max_intensity
               f_time[tt,n,i] = time[tt]
               f_index[tt,n,i] = i + 1
      else: 
         for i in range(0, len(f_props)):
            cx_index = f_props[i].centroid[1]
            cy_index = f_props[i].centroid[0]
            f_cent_x[tt,n,i] = gridpoint_interp(x, cx_index, cy_index)
            f_cent_y[tt,n,i] = gridpoint_interp(y, cx_index, cy_index)
            f_area[tt,n,i] = f_props[i].area
            f_max_intensity[tt,n,i] = f_props[i].max_intensity
            f_time[tt,n,i] = time[tt]
            f_index[tt,n,i] = i + 1

###################################### Match Forecast ('f') and verification ('v') Objects: #######################################################

print 'match objects part'

masked_v_cent_x = np.ma.masked_array(v_cent_x, v_cent_x==0)
masked_v_cent_y = np.ma.masked_array(v_cent_y, v_cent_y==0)
masked_v_intensity = np.ma.masked_array(v_max_intensity, v_max_intensity==0)
masked_v_area = np.ma.masked_array(v_area, v_area==0)
masked_v_time = np.ma.masked_array(v_time, v_time==0)

v_cent_x_flat = masked_v_cent_x.flatten()
v_cent_y_flat = masked_v_cent_y.flatten()
v_intensity_flat = masked_v_intensity.flatten()
v_area_flat = masked_v_area.flatten()
v_time_flat = masked_v_time.flatten()

masked_f_cent_x = np.ma.masked_array(f_cent_x, f_cent_x==0)
masked_f_cent_y = np.ma.masked_array(f_cent_y, f_cent_y==0)
masked_f_intensity = np.ma.masked_array(f_max_intensity, f_max_intensity==0)
masked_f_area = np.ma.masked_array(f_area, f_area==0)
masked_f_time = np.ma.masked_array(f_time, f_time==0)
masked_f_index = np.ma.masked_array(f_index, f_index==0)

ti = np.zeros((len(time),ne,ob_max))
time_score = np.zeros((len(time),ne,ob_max))
dist_score = np.zeros((len(time),ne,ob_max))
cent_dis_x = np.zeros((len(time),ne,ob_max))
cent_dis_y = np.zeros((len(time),ne,ob_max))
cent_dis = np.zeros((len(time),ne,ob_max))
match_intensity = np.zeros((len(time),ne,ob_max))
match_area = np.zeros((len(time),ne,ob_max))
match_index = np.zeros((len(time),ne,ob_max))
match_f_index = np.zeros((len(time),ne,ob_max))

#print 'time is: ', time
#print 'v time flat is: ', v_time_flat, np.max(v_time_flat)
#print 'v time is: ', v_time, np.max(v_time)
#print 'vtimes is: ', vtimes

for n in range(0, ne):
   for tt in range(0, len(time)):
      for i in range(0, f_cent_x.shape[2]):
         if(masked_f_cent_x[tt,n,i]):
            cent_x_diff_temp = (f_cent_x[tt,n,i] - v_cent_x_flat) / 1000.
            cent_y_diff_temp = (f_cent_y[tt,n,i] - v_cent_y_flat) / 1000.
            cent_diff_temp = np.sqrt(cent_x_diff_temp**2 + cent_y_diff_temp**2)
            time_diff_temp = abs(time[tt] - v_time_flat)
            cent_diff_temp = np.where(cent_diff_temp > cutoff_rad,cutoff_rad,cent_diff_temp)
            time_diff_temp = np.where(time_diff_temp > cutoff_time,cutoff_time,time_diff_temp)
            dist_score_temp = (cutoff_rad - cent_diff_temp) / cutoff_rad
            time_score_temp = (cutoff_time - time_diff_temp) / cutoff_time
            ti_temp = dist_score_temp * time_score_temp
            best_index = np.argmax(ti_temp)
            ti[tt,n,i] = ti_temp[best_index]
            time_score[tt,n,i] = time_score_temp[best_index]
            dist_score[tt,n,i] = dist_score_temp[best_index]
            cent_dis_x[tt,n,i] = cent_x_diff_temp[best_index]
            cent_dis_y[tt,n,i] = cent_y_diff_temp[best_index]
            cent_dis[tt,n,i] = cent_diff_temp[best_index]
            match_intensity[tt,n,i] = v_intensity_flat[best_index]
            match_area[tt,n,i] = v_area_flat[best_index]
            match_f_index[tt,n,i] = 1

cent_dis_x = np.where(np.isnan(cent_dis_x), 0., cent_dis_x)
cent_dis_y = np.where(np.isnan(cent_dis_y), 0., cent_dis_y)
cent_dis = np.where(np.isnan(cent_dis), 0., cent_dis)
match_intensity = np.where(np.isnan(match_intensity), 0., match_intensity)
match_area = np.where(np.isnan(match_area), 0., match_area)
match_f_index = np.where(np.isnan(match_f_index), 0., match_f_index)

#################################### Calc Object-based Threat Score (OTS): #############################################

match_f_index = np.where(ti < score_thresh, 0, 1)
match_ti = np.where(ti < score_thresh, 0., ti)
f_hit_area = np.where(ti < score_thresh, 0., f_area)
v_hit_area = np.where(ti < score_thresh, 0., match_area)

weighted_hit_area = match_ti * (f_hit_area + v_hit_area)
hit_area = f_hit_area + v_hit_area

total_weighted_hit_area = weighted_hit_area.sum(axis=2)

total_hit_area = hit_area.sum(axis=2)

total_area = np.zeros((ti.shape[0],ne))

v_hit_area_sum = v_hit_area.sum(axis=2)
v_area_sum = v_area.sum(axis=1)
f_area_sum = f_area.sum(axis=2)

#### Needed to set v area to area of missed v objects ###

for tt in range(0, len(time)):
   for n in range(0, ne):
      if (v_hit_area_sum[tt,n] == 0.):
         total_area[tt,n] = v_area_sum[tt] + f_area_sum[tt,n]
      else:
         total_area[tt,n] = v_hit_area_sum[tt,n] + f_area_sum[tt,n]

total_area = np.where(total_area == 0., 0.1, total_area)

ots = 1. / total_area * total_weighted_hit_area
binary_ots = 1. / total_area * total_hit_area

mean_ots = np.mean(ots, axis=1)
mean_bin_ots = np.mean(binary_ots, axis=1)

############################ Calc Match vs. FA probabilities: #############################################

hits = fcst_var * 0.
fas = fcst_var * 0.

for tt in range(0, len(time)):
   for n in range(0, ne):
      for i in range(0, f_cent_x.shape[2]):
         if (match_f_index[tt,n,i] > 0): 
            hits[n,tt,:,:] = np.where(f_labels[n,tt,:,:] == masked_f_index[tt,n,i], 1., hits[n,tt,:,:])
         else: 
            fas[n,tt,:,:] = np.where(f_labels[n,tt,:,:] == masked_f_index[tt,n,i], 1., fas[n,tt,:,:])

hit_prob = np.sum(hits, axis=0) / ne
fa_prob = np.sum(fas, axis=0) / ne

masked_hit_prob = np.ma.masked_array(hit_prob, hit_prob<0.05)
masked_fa_prob = np.ma.masked_array(fa_prob, fa_prob<0.05)

###################################### Make Plots: #######################################################

print 'plot part'

if (var == 'DZ'): 
   ob_ver_plot(map, fig, ax1, ax2, ax3, x, y, vx, vy, object_plot, masked_hit_prob[t,:,:], masked_fa_prob[t,:,:], plot_dz, radmask, t, mean_ots[t], mean_bin_ots[t], init_label, valid_label, domain, outdir, 5, 0)
else: 
   ob_ver_plot(map, fig, ax1, ax2, ax3, x, y, vx, vy, object_plot, masked_hit_prob[t,:,:], masked_fa_prob[t,:,:], verif_var[vt,:,:], radmask, t, mean_ots[t], mean_bin_ots[t], init_label, valid_label, domain, outdir, 5, 0)

##   ob_cent_plot(map, fig, ax1, ax2, ax3, masked_f_cent_x[t,:,:], masked_f_cent_y[t,:,:], vx, vy, cent_plot, ti[t,:,:], verif_var[vt,:,:], t, init_label, valid_label, domain, outdir, 5, 0)

