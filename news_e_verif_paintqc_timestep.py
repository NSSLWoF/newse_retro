#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.basemap import Basemap
from scipy import signal
from scipy import *
from scipy import ndimage
import skimage
from skimage.morphology import label
from skimage.measure import regionprops
import math
from math import radians, tan, sin, cos, pi, atan, sqrt, pow, asin, acos
import pylab as P
import numpy as np
from numpy import NAN
import sys
import netCDF4
from optparse import OptionParser
from netcdftime import utime
import os
import time as timeit
import ctables
import news_e_post_cbook
from news_e_post_cbook import *
import news_e_plotting_cbook_v2
from news_e_plotting_cbook_v2 import *

sys.path.append("/scratch/software/anaconda/bin")

####################################### File Variables: ######################################################

parser = OptionParser()
parser.add_option("-d", dest="exp_dir", type="string", default= None, help="Input Directory (of summary files)")
parser.add_option("-o", dest="outdir", type="string", help = "Output Directory (for images)")
parser.add_option("-w", dest="warn_path", type="string", help = "Path to Shapefile of Warning Products")
parser.add_option("-l", dest="lsr_path", type="string", help = "Path to Shapefile of LSR Products")
parser.add_option("-z", dest="verif_file_dz", type="string", default= None, help="Input DZ file for verification data")
parser.add_option("-a", dest="verif_file_aws", type="string", default= None, help="Input AWS file for verification data")
parser.add_option("-m", dest="verif_file_aws_ml", type="string", default= None, help="Input ML AWS file for verification data")
parser.add_option("-t", dest="t", type="int", help = "Timestep to process")

(options, args) = parser.parse_args()

if ((options.exp_dir == None) or (options.outdir == None) or (options.warn_path == None) or (options.lsr_path == None) or (options.verif_file_dz == None) or (options.verif_file_aws == None) or (options.verif_file_aws_ml == None) or (options.t == None)):
   print
   parser.print_help()
   print
   sys.exit(1)
else:
   exp_dir = options.exp_dir
   outdir = options.outdir
   warn_path = options.warn_path
   lsr_path = options.lsr_path
   verif_file_dz = options.verif_file_dz
   verif_file_aws = options.verif_file_aws
   verif_file_aws_ml = options.verif_file_aws_ml
   t = options.t

domain = 'full'

#################################### User-Defined Variables:  #####################################################

edge            = 7 		#number of grid points to remove from near domain boundaries
thin		= 6		#thinning factor for quiver values (e.g. 6 means slice every 6th grid point)

radius_max      = 3		#grid point radius for maximum value filter
radius_gauss	= 2		#grid point radius of convolution operator 
neighborhood 	= 15		#grid point radius of prob matched mean neighborhood

aws_thresh      = 0.0001        #hacks to contour objects
wz_thresh       = 0.0001
dz_thresh       = 40. 

plot_alpha 	= 0.6		#transparency value for filled contour plots

#################################### Colormap Names:  #####################################################

paintball_colors = [cb_colors.q1, cb_colors.q2, cb_colors.q3, cb_colors.q4, cb_colors.q5, cb_colors.q6, cb_colors.q7, cb_colors.q8, cb_colors.q9, cb_colors.q10, cb_colors.q11, cb_colors.q12, cb_colors.purple3, cb_colors.purple4, cb_colors.green3, cb_colors.green4, cb_colors.orange3, cb_colors.orange4]

#################################### Basemap Variables:  #####################################################

resolution 	= 'h'
area_thresh 	= 1000.

damage_files = '' #['/Volumes/fast_scr/pythonletkf/vortex_se/2013-11-17/shapefiles/extractDamage_11-17/extractDamagePaths']

#################################### Initialize plot attributes using 'v plot' objects:  #####################################################

aws_wz_paint_plot = v_plot('aws_wz0to2_paintqc',                   \
                   '3-km MRMS 0-2 km Az. Wind Shear Objects',                  \
                   'NEWS-e Member 0-2 km Vertical Vorticity Objects',                        \
                   cb_colors.gray6,   \
                   cb_colors.green4,     \
                   [aws_thresh, 1000.],              \
                   [wz_thresh, 1000.],     \
                   [cb_colors.gray8],                      \
                   paintball_colors,                     \
                   '',  \
                   '',  \
                   'max',  \
                   0.6, \
                   neighborhood)

aws_uh0to2_paint_plot = v_plot('aws_uh0to2_paintqc',                   \
                   '3-km MRMS 0-2 km Az. Wind Shear Objects',                  \
                   'NEWS-e Member 0-2 km Updraft Helicity Objects',                        \
                   cb_colors.gray6,   \
                   cb_colors.green4,     \
                   [aws_thresh, 1000.],              \
                   [wz_thresh, 1000.],     \
                   [cb_colors.gray8],                      \
                   paintball_colors,                     \
                   '',  \
                   '',  \
                   'max',  \
                   0.6, \
                   neighborhood)

aws_uh2to5_paint_plot = v_plot('aws_uh2to5_paintqc',                   \
                   '3-km MRMS 2-5 km Az. Wind Shear Objects',                  \
                   'NEWS-e Member 2-5 km Updraft Helicity Objects',                        \
                   cb_colors.gray6,   \
                   cb_colors.green4,     \
                   [aws_thresh, 1000.],              \
                   [wz_thresh, 1000.],     \
                   [cb_colors.gray8],                      \
                   paintball_colors,                     \
                   '',  \
                   '',  \
                   'max',  \
                   0.6, \
                   neighborhood)

dz_paint_plot = v_plot('dz_paintqc',                   \
                   '3-km MRMS 40-dBZ Composite Reflectivity',                  \
                   'NEWS-e Member 40-dBZ Composite Reflectivity',                        \
                   cb_colors.gray6,   \
                   cb_colors.green4,     \
                   [dz_thresh, 1000.],              \
                   [dz_thresh, 1000.],     \
                   [cb_colors.gray8],                      \
                   paintball_colors,                     \
                   '',  \
                   '',  \
                   'max',  \
                   0.6, \
                   neighborhood)


######################################################################################################
#################################### Read Data:  #####################################################
######################################################################################################

##################### Get list of summary files to process: ##############################

files = []
files_temp = os.listdir(exp_dir)
for f, file in enumerate(files_temp):
   if (file[0] == '2'):
      files.append(file)

files.sort()
ne = len(files)

############### for each ensemble member summary file: #############################

for f, file in enumerate(files):
   exp_file = os.path.join(exp_dir, file)

   try:
      fin = netCDF4.Dataset(exp_file, "r")
      print "Opening %s \n" % exp_file
   except:
      print "%s does not exist! \n" % exp_file
      sys.exit(1)

############## Get grid/forecast time information from first summary file: ##################

   if (f == 0):

############# Get/process date/time info, handle 00Z shift for day but not month/year ############

      date = file[0:10]
      init_date = date
      init_label = 'Init: ' + date + ', ' + file[11:13] + file[14:16] + ' UTC'
      init_hr = int(file[11:13])

      time_full = fin.variables['TIME'][:]
      init_time = fin.variables['TIME'][0]
      time = fin.variables['TIME'][t]
#      if (time is ma.masked):
#         time = fin.variables['TIME'][t-1] + 300.

      if (time < 35000.):
         time_correct = time + 86400. 
      else: 
         time_correct = time 

      valid_hour = np.floor(time / 3600.)
      valid_min = np.floor((time - valid_hour * 3600.) / 60.)

      if (valid_hour > 23):
         valid_hour = valid_hour - 24
         if (init_hr > 20):
            temp_day = int(date[-2:])+1
            temp_day = str(temp_day)
            if (len(temp_day) == 1):
               temp_day = '0' + temp_day
            date = date[:-2] + temp_day

      valid_hour = str(int(valid_hour))
      valid_min = str(int(valid_min))

      if (len(valid_hour) == 1):
         valid_hour = '0' + valid_hour
      if (len(valid_min) == 1):
         valid_min = '0' + valid_min

      valid_label = 'Valid: ' + date + ', ' + valid_hour + valid_min + ' UTC'

############ Get grid/projection info, chop 'edge' from boundaries: ####################

      xlat = fin.variables['XLAT'][:]
      xlon = fin.variables['XLON'][:]
      xlat = xlat[edge:-edge,edge:-edge]
      xlon = xlon[edge:-edge,edge:-edge]

      sw_lat_full = xlat[0,0]
      sw_lon_full = xlon[0,0]
      ne_lat_full = xlat[-1,-1]
      ne_lon_full = xlon[-1,-1]

      cen_lat = fin.CEN_LAT
      cen_lon = fin.CEN_LON
      stand_lon = fin.STAND_LON
      true_lat1 = fin.TRUE_LAT1
      true_lat2 = fin.TRUE_LAT2

######################### Initialize variables: ####################################

      dz = np.zeros((ne, xlat.shape[0], xlat.shape[1]))

######################## Read in summary file variables: ################################

   dz[f,:,:] = fin.variables['DZ_COMP'][t,edge:-edge,edge:-edge]

   fin.close()
   del fin

#dz_plot = dz[t,:,:]
#dz_plot = np.where(dz >= dz_thresh, dz, 0.) #threshold current timestep dz

############# Get/process Initial date/time info, handle 00Z shift for day but not month/year ############

start_hour = np.floor(time / 3600.)
start_min = np.floor((time - start_hour * 3600.) / 60.)

str_hour = str(int(start_hour))
str_min = str(int(start_min))

if (len(str_hour) == 1):
   str_hour = '0' + str_hour
if (len(str_min) == 1):
   str_min = '0' + str_min

############# Get/process forecast rotation object info - rot_file name is hard coded ############

rot_file = os.path.join(exp_dir, 'rot_qc_final.nc')
#rot_file = os.path.join(exp_dir, 'rot_qc.nc')

try:
   rot_in = netCDF4.Dataset(rot_file, "r")
   print "Opening %s \n" % rot_file
except:
   print "%s does not exist! \n" % rot_file
   sys.exit(1)

wz_0to2 = rot_in.variables['WZ_0TO2_WINDOW_MASK'][:,t,edge:-edge,edge:-edge]
uh_0to2 = rot_in.variables['UH_0TO2_WINDOW_MASK'][:,t,edge:-edge,edge:-edge]
uh_2to5 = rot_in.variables['UH_2TO5_WINDOW_MASK'][:,t,edge:-edge,edge:-edge]

rot_in.close()
del rot_in

#################################### Get/process low-level verification rotation object info  #####################################################

try:
   aws_in = netCDF4.Dataset(verif_file_aws, "r")
   print "Opening %s \n" % verif_file_aws
except:
   print "%s does not exist! \n" % verif_file_aws
   sys.exit(1)

vlat = aws_in.variables['XLAT'][:]
vlon = aws_in.variables['XLON'][:]

vlat = vlat[edge:-edge,edge:-edge]
vlon = vlon[edge:-edge,edge:-edge]

vtimes = aws_in.variables['TIME'][:]

vt = (np.abs(time_correct - vtimes)).argmin()

radmask = aws_in.variables['RADMASK'][:]
radmask = radmask[edge:-edge,edge:-edge]

aws_qc = aws_in.variables['VAR_CRESS_WINDOW_QC'][vt,edge:-edge,edge:-edge]

aws_in.close()
del aws_in

#################################### Get/process midlevel verification rotation object info  #####################################################

try:
   aws_in = netCDF4.Dataset(verif_file_aws_ml, "r")
   print "Opening %s \n" % verif_file_aws_ml
except:
   print "%s does not exist! \n" % verif_file_aws_ml
   sys.exit(1)

mlat = aws_in.variables['XLAT'][:]
mlon = aws_in.variables['XLON'][:]

mlat = vlat[edge:-edge,edge:-edge]
mlon = vlon[edge:-edge,edge:-edge]

mtimes = aws_in.variables['TIME'][:]

mt = (np.abs(time_correct - mtimes)).argmin()

aws_qc_ml = aws_in.variables['VAR_CRESS_WINDOW_QC'][mt,edge:-edge,edge:-edge]

aws_in.close()
del aws_in

#################################### Get/process verification reflectivity info  #####################################################

try:
   dz_in = netCDF4.Dataset(verif_file_dz, "r")
   print "Opening %s \n" % verif_file_dz
except:
   print "%s does not exist! \n" % verif_file_dz
   sys.exit(1)

zlat = dz_in.variables['XLAT'][:]
zlon = dz_in.variables['XLON'][:]

zlat = zlat[edge:-edge,edge:-edge]
zlon = zlon[edge:-edge,edge:-edge]

ztimes = dz_in.variables['TIME'][:]

zt = (np.abs(time_correct - ztimes)).argmin()

mrms_dz_cressman = dz_in.variables['VAR_CRESSMAN'][zt,edge:-edge,edge:-edge]

########################### Apply radmask to DZ fields: ##############################

dz_plot = dz * 0.
for n in range(0, ne): 
   dz_plot[n,:,:] = np.where(radmask > 0, 0., dz[n,:,:])

mrms_dz_cressman = np.where(radmask > 0, 0., mrms_dz_cressman)

dz_in.close()
del dz_in

################################# Make Figure Template: ###################################################

print 'basemap part'

map, fig, ax1, ax2, ax3 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh, verif='True')

x, y = map(xlon[:], xlat[:])
xx, yy = map.makegrid(xlat.shape[1], xlat.shape[0], returnxy=True)[2:4]   #equidistant x/y grid for streamline plots

vx, vy = map(vlon[:], vlat[:])

##########################################################################################################
###################################### Make Plots: #######################################################
##########################################################################################################

print 'plot part'

############################## wz0to2 paintball plot: ################################

hail, wind, tornado = plot_lsr(map, fig, ax1, ax2, ax3, lsr_path, init_time, time_correct)
svr, tor = plot_warn(map, fig, ax1, ax2, ax3, warn_path, time_correct, cb_colors.blue6, cb_colors.red6)

paintqc_plot(map, fig, ax1, ax2, ax3, x, y, vx, vy, aws_wz_paint_plot, aws_qc, wz_0to2, radmask, t, init_label, valid_label, domain, outdir, 5, 0)

remove_warn(svr)
remove_warn(tor)
remove_lsr(hail, wind, tornado)

###########################################################################################################
### make new figure for each paint plot ... can't figure out how to remove each members plot from basemap
###########################################################################################################

print 'basemap part, part 2 - the basemappening'

############################## uh0to2 paintball plot: ################################

map2, fig2, ax21, ax22, ax23 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh, verif='True')

hail, wind, tornado = plot_lsr(map2, fig2, ax21, ax22, ax23, lsr_path, init_time, time_correct)
svr, tor = plot_warn(map, fig2, ax21, ax22, ax23, warn_path, time_correct, cb_colors.blue6, cb_colors.red6)

paintqc_plot(map2, fig2, ax21, ax22, ax23, x, y, vx, vy, aws_uh0to2_paint_plot, aws_qc, uh_0to2, radmask, t, init_label, valid_label, domain, outdir, 5, 0)

remove_warn(svr)
remove_warn(tor)
remove_lsr(hail, wind, tornado)

print 'basemap part, part 3 - So very tired ...'

############################## uh2to5 paintball plot: ################################

map3, fig3, ax31, ax32, ax33 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh, verif='True')

hail, wind, tornado = plot_lsr(map3, fig3, ax31, ax32, ax33, lsr_path, init_time, time_correct)
svr, tor = plot_warn(map3, fig3, ax31, ax32, ax33, warn_path, time_correct, cb_colors.blue6, cb_colors.red6)

paintqc_plot(map3, fig3, ax31, ax32, ax33, x, y, vx, vy, aws_uh2to5_paint_plot, aws_qc_ml, uh_2to5, radmask, t, init_label, valid_label, domain, outdir, 5, 0)

remove_warn(svr)
remove_warn(tor)
remove_lsr(hail, wind, tornado)

print 'basemap part, part 4 - Citizens on Patrol'

############################## dz paintball plot: ################################

map4, fig4, ax41, ax42, ax43 = create_fig(sw_lat_full, sw_lon_full, ne_lat_full, ne_lon_full, true_lat1, true_lat2, cen_lat, stand_lon, damage_files, resolution, area_thresh, verif='True')

hail, wind, tornado = plot_lsr(map4, fig4, ax41, ax42, ax43, lsr_path, init_time, time_correct)
svr, tor = plot_warn(map4, fig4, ax41, ax42, ax43, warn_path, time_correct, cb_colors.blue6, cb_colors.red6)

paintqc_plot(map4, fig4, ax41, ax42, ax43, x, y, vx, vy, dz_paint_plot, mrms_dz_cressman, dz_plot, radmask, t, init_label, valid_label, domain, outdir, 5, 0)

remove_warn(svr)
remove_warn(tor)
remove_lsr(hail, wind, tornado)

