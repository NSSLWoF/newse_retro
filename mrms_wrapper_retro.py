###################################################################################################

from mpl_toolkits.basemap import Basemap
import matplotlib
import math
from scipy import *
import pylab as P
import numpy as np
import sys, glob
import os
import time
from optparse import OptionParser
import netCDF4
from news_e_post_cbook import *

from multiprocessing import Pool

sys.path.append("/scratch/software/anaconda/bin")

#=======================================================================================================================
# run_script is a function that runs a system command

def run_script(cmd):
    print "Executing command:  " + cmd
    os.system(cmd)
    print cmd + "  is finished...."
    return

###################################################################################################

parser = OptionParser()
parser.add_option("-d", dest="exp_dir", type="string", default=None, help="Input Directory (summary files)")
parser.add_option("-i", dest="image_dir", type="string", default=None, help="Image Directory")
parser.add_option("-w", dest="warn_path", type="string", help = "Path to Shapefile of Warning Products")
parser.add_option("-l", dest="lsr_path", type="string", help = "Path to Shapefile of LSR Products")
parser.add_option("-z", dest="verif_file_dz", type="string", default= None, help="Input DZ file for verification data")
parser.add_option("-a", dest="verif_file_aws", type="string", default= None, help="Input AWS file for verification data")
parser.add_option("-m", dest="verif_file_aws_ml", type="string", default= None, help="Input ML AWS file for verification data")

(options, args) = parser.parse_args()

if ((options.exp_dir == None) or (options.image_dir == None) or (options.warn_path == None) or (options.lsr_path == None) or (options.verif_file_dz == None) or (options.verif_file_aws == None) or (options.verif_file_aws_ml == None)):
    print
    parser.print_help()
    print
    sys.exit(1)
else:
    exp_dir = options.exp_dir
    image_dir = options.image_dir
    warn_path = options.warn_path
    lsr_path = options.lsr_path
    verif_file_dz = options.verif_file_dz
    verif_file_aws = options.verif_file_aws
    verif_file_aws_ml = options.verif_file_aws_ml

################## Get number of forecast times (fcst_nt) from first summary file: ########################

files = []
files_temp = os.listdir(exp_dir)
for f, file in enumerate(files_temp):
   if (file[0] == '2'):
      files.append(file)

exp_file = os.path.join(exp_dir, files[0])

try:
   fin = netCDF4.Dataset(exp_file, "r")
   print "Opening %s \n" % exp_file
except:
   print "%s does not exist! \n" % exp_file
   sys.exit(1)

temp_time = fin.variables['TIME'][:]
fcst_nt = len(temp_time)

print 'nt is: ', fcst_nt

fin.close()
del fin

############### Probability matched mean composite reflectivity path is hard coded: #################

pmm_file = os.path.join(exp_dir, 'pmm_dz.nc')

pool = Pool(processes=(32))              # set up a queue to run

for t in range(0, fcst_nt):

   cmd = "python mrms_timestep_retro.py -d %s -o %s -w %s -l %s -z %s -a %s -m %s -t %d" % (exp_dir, image_dir, warn_path, lsr_path, verif_file_dz, verif_file_aws, verif_file_aws_ml, t)
   pool.apply_async(run_script, (cmd,))
   time.sleep(2)

time.sleep(10)
 
pool.close()
pool.join()


